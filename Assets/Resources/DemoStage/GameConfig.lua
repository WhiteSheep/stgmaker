require "StgMaker\\CLStdLib"

--	注册游戏环境
CL.RegistSystemEnv(Env.ProjectPath, "DemoStage/")

--	载入游戏资源
CL.LoadTexture2D("player", "pl00.png")
CL.LoadTexture2D("enemy", "enemy.png")
CL.LoadTexture2D("explode", "boom3_0.png")

--[===[ 以下代码都用于开发测试  ]===]
--	NameSpace Test
print("EventType is:", EventType) 
print("BuildInVirtualButton is :", BuildInVirtualButton)
print("BuildInAnimationStateType is :", BuildInAnimationStateType)
print("SpriteObjectType is:", SpriteObjectType)
print("ColliderType is:", ColliderType)
print("Collider is:",Collider)
print("BuildInAnimationControllerType is :", BuildInAnimationControllerType)
print("SpriteObjectCampType is :",SpriteObjectCampType)

--	Collider Test
local collider = Collider.New()
collider.type = ColliderType.Circle
collider.offset = Vector2.New(0,0)
collider.size = Vector2.New(0.05, 0.05)
collider.radius = 0.4

print("Collder2D - Test")
print("collider.type = ",collider.type)
print("collider.offset = ",collider.offset)
print("collider.size = ",collider.size)
print("collider.radius = ",collider.radius)

