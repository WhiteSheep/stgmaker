require "StgMaker\\CLStdLib"

function Main(...)

	--	测试参数
	local a,b = ...
	print(a,b)

	--	创建一个Wrapper
	local wrapper = SpriteObjectWrapper.New()

	--	创建一个精灵单位
	--	首先需要做的工作是，调用SpriteObject.New方法创建一个精灵，然后可以随意初始化精灵的参数
	local spriteUnit = SpriteObject.New()

	spriteUnit.hp = 100
	spriteUnit.atk = 10
	spriteUnit.camp = SpriteObjectCampType.Confederate
	spriteUnit.position = Vector2.New(0, 0)
	spriteUnit.type = SpriteObjectType.SpriteEffect
	wrapper:RegistSpriteObject(spriteUnit)

	--[==[
	创建好的精灵需要拥有自己的帧动画，以便在游戏中显示，因此调用AnimationGroup.Create创建一个动画组
		local animationGroup = AnimationGroup.Create()
	----------------
	之后，我们需要设定该动画组的类型，在这里选择系统自带的默认敌人类型
		animationGroup:SetControllerType(AnimationControllerType.SimpleEnemy)
	----------------
	这样，便可以向动画组添加动画了，首先看代码：
		animationGroup:AddAnimationClip(BuildInAnimationStateType.Idle, AnimationClip.CreateWithGrid("player", 32, 48, 0, 8))
	动画组中的AddAnimationClip是添加动画片段的方法，它接受两个参数，第一个是该片段对应的动画状态机类型，第二个是动画片段，样例代码中，我们准备设定单位在闲置状态（Idle）时应该播放怎样的动画片段，第二个参数又是一个Create：
		AnimationClip.CreateWithGrid("player", 32, 48, 0, 8)
		在这里AnimationClip.CreateWithGrid是创建一个动画片段，它接受5个参数，分别是：图片ID、动画帧宽度、动画帧高度、起始帧、帧长度
	
	这样一来，就可以创建动画并绑定在对应的状态上，最后我们调用：
		sprite:RegistAnimationGroup(animationGroup)
	就将动画绑定在精灵身上了
	]==]

	local animationGroup = AnimationGroup.Create()
	animationGroup:SetControllerType(BuildInAnimationControllerType.SimpleSprite)
	animationGroup:AddAnimationClip(BuildInAnimationStateType.Idle, AnimationClip.CreateWithGrid("player", 32, 48, 0, 8))
	animationGroup:AddAnimationClip(BuildInAnimationStateType.ToLeft, AnimationClip.CreateWithGrid("player", 32, 48, 8, 4))
	animationGroup:AddAnimationClip(BuildInAnimationStateType.Left, AnimationClip.CreateWithGrid("player", 32,48, 12, 4))
	animationGroup:AddAnimationClip(BuildInAnimationStateType.ToRight, AnimationClip.CreateWithGrid("player", 32,48, 16, 4))
	animationGroup:AddAnimationClip(BuildInAnimationStateType.Right, AnimationClip.CreateWithGrid("player", 32,48, 20, 4))
	wrapper:RegistAnimationGroup(animationGroup)

	--[==[
	为了能够让单位拥有碰撞判定，我们需要创建碰撞体，并将其绑定在单位上
	系统中提供了方形、圆形、多边形和折线四种碰撞体供设计者使用，下面的Demo是创建了一个半径为8像素的圆形碰撞体
	]==]
	local collider = Collider.New()
	collider.type = ColliderType.Circle
	collider.radius = 0.08
	wrapper:RegistColliders(collider)

	--[==[
	为了让精灵在场景中能够运动或者是做一些其他事情，我们需要创建触发器，触发器可以理解为：当XX事件发生时，如果满足YY条件，就执行ZZ活动。一般来说，你想要实现的一些内容都可以通过触发器表述出来，如发射弹幕、移动等
	
	首先，通过如下代码创建一个触发器
		local trigger = Trigger.Create()
	创建完成后，我们需要对触发器赋予事件、条件、行为部分
	
	1、事件部分
		trigger:AddEvent(Event.Create(EventType.Period, 120))
	通过AddEvent方法就可以绑定事件了，demo中创建了一个周期性事件（EventType.Period），并设定了周期时间，这意味着该触发器每隔120帧便会触发一次
	
	2、条件部分
		trigger:AddCondition(function() return true end)
	通过AddCondition方法就可以绑定条件了，demo中绑定了一个函数（条件必须是函数，返回值是true or false），当触发器触发时，如果该条件结果为true，则准备执行行为
	
	3、行为部分
		trigger:AddAction(Action.Create(	
			function(e, ...)
				XXXXXXXX codes
				XXXXXXXX codes
			end
		,1,2,3))
	首先注意，通过AddAction来绑定行为部分，行为通过Action.Create来创建，该方法接受一个方法和一系列参数，这些参数会在行为函数运行时，作为参数传入到函数中。在demo中，行为函数后面跟了1、2、3三个整数，这些整数在运行时会传入函数中
	
	另一方面，行为函数需要在自己需要的参数前加一个变量（demo中叫做e），这是为了接收一些有用的信息的，后面会看到用处
	这样一来，当发生事件、条件满足时，行为函数便会执行，现在再来看看行为函数内部都能做些什么
	--------------------------------
	function(e, ...)
		print("Params:", ...)
		for i = 1, 3 do
			print("Frame at: ",CL.GetFrameCount(),", id=", e.binderId)
			coroutine.yield(20)
		end
	end
	
	上面的行为函数需要关注coroutine.yield(20)这句话，假设我们有一个需求，每隔20帧创建一个敌人，一共创建3次，就需要这句话了。coroutine.yield执行时，脚本会在此处暂停，暂停时间根据传入的参数决定，当到达时间后，脚本会从此处继续执行，上面的demo中，循环每运行一轮会等待20帧，一共3次，也就是60帧
	--------------------------------
	至此，一个触发器就做好了，和动画组一样，最后我们要把触发器绑定在精灵上：
		sprite:RegistTrigger(trigger, triggerKeyDown)
	Tips：RegistTrigger一次可以绑定1个以上触发器
	
]==]
	local triggerKeyDown = Trigger.New()
	triggerKeyDown:AddEvents(Event.New(EventType.OnKeyPressed))
	triggerKeyDown:AddActions(Action.New(
		function (e)
			local this = e.binder
			local nowPos = this.position
			local speed = 0.05
			
			if (Input.GetButtonPressed(BuildInVirtualButton.Up)) then
				nowPos.y = nowPos.y + speed
			end
			
			if (Input.GetButtonPressed(BuildInVirtualButton.Down)) then
				nowPos.y = nowPos.y - speed
			end
			
			if (Input.GetButtonPressed(BuildInVirtualButton.Left)) then
				nowPos.x = nowPos.x - speed
			end
			
			if (Input.GetButtonPressed(BuildInVirtualButton.Right)) then
				nowPos.x = nowPos.x + speed
			end
			
			this.position = nowPos

		end
	))

	wrapper:RegistTrigger(triggerKeyDown)

	-- 最后返回创建好的Wrapper即可
	return wrapper
end

--	需要注意的是，整个方法需要返回以便系统识别和调用
return Main
