require "StgMaker\\CLStdLib"

function Main(...)

	local wrapper = SpriteObjectWrapper.New()

	local enemySprite = SpriteObject.New()
	enemySprite.type = SpriteObjectType.SpriteUnit
	enemySprite.atk = 0
	enemySprite.hp = 200
	enemySprite.camp = SpriteObjectCampType.Opponent
	enemySprite.position = Vector2.New(0, 0)
	wrapper:RegistSpriteObject(enemySprite)

	--[==[
		另一个AnimationClip生成方案的测试脚本，该demo中，精灵动画片段采用CustomedRect方式生成
	]==]
	local animGroup = AnimationGroup.Create()
	animGroup:SetControllerType(BuildInAnimationControllerType.SimpleSprite)
	animGroup:AddAnimationClip(BuildInAnimationStateType.Idle, AnimationClip.CreateWithCustomedRect("enemy", 0, Rect.Create(0, 224, 32, 32, SliceDirection.Horizonal, 4)))
	animGroup:AddAnimationClip(BuildInAnimationStateType.ToLeft, AnimationClip.CreateWithCustomedRect("enemy", 0, Rect.Create(256, 256, 32, 32, SliceDirection.Horizonal, 4)))
	animGroup:AddAnimationClip(BuildInAnimationStateType.Left, AnimationClip.CreateWithCustomedRect("enemy", 0, Rect.Create(384, 256, 32, 32, SliceDirection.Horizonal, 4)))
	animGroup:AddAnimationClip(BuildInAnimationStateType.ToRight, AnimationClip.CreateWithCustomedRect("enemy", 0, Rect.Create(128, 224, 32, 32, SliceDirection.Horizonal, 4)))
	animGroup:AddAnimationClip(BuildInAnimationStateType.Right, AnimationClip.CreateWithCustomedRect("enemy", 0, Rect.Create(256, 224, 32, 32, SliceDirection.Horizonal, 4)))

	wrapper:RegistAnimationGroup(animGroup)

	--[==[
		下面的碰撞代码中，我们为敌人添加了本系统中所支持的所有碰撞体，分别为Circle、Box、Polygon以及Edge
	]==]
		local colliderCircle = Collider.New()
		colliderCircle.type = ColliderType.Circle
		colliderCircle.radius = 1.5

		local colliderBox = Collider.New()
		colliderBox.type = ColliderType.Box
		colliderBox.offset = Vector2.New(0, 0)
		colliderBox.size = Vector2.New(1.5, 1.5)

		local colliderPolygon = Collider.New()
		colliderPolygon.type = ColliderType.Polygon
		colliderPolygon.pathCount = 1
		colliderPolygon:AddPathPoints(0, Vector2.New(-2, 1), Vector2.New(1, 2), Vector2.New(2, -1), Vector2.New(-1, -2))

		local colliderEdge = Collider.New()
		colliderEdge.type = ColliderType.Edge
		colliderEdge:AddEdgePoints(Vector2.New(-2, 1),Vector2.New(-1, -1),Vector2.New(0, 1),Vector2.New(1, -1),Vector2.New(2, 1))
		
		wrapper:RegistColliders(colliderCircle, colliderBox, colliderPolygon, colliderEdge)

	--[==[
		下面的Trigger Demo，针对精灵的位移给出了两种不同的写法， 系统推荐第一种写法，主要是由于第二种写法会反复创建新的LuaFunction，这会导致效率比较低，不过性能仍在可接受水平，开发人员请根据需要自行选择
	]==]

	local trigger = Trigger.New()
	trigger:AddEvents(Event.New(EventType.OnSpriteLifeTimeElapse, 60, enemySprite.id))
	trigger:AddActions(Action.New(
		function(e)
			--local this = RuntimeHelper.GetSpriteObjectByID(e.binderId)
			local this = e.binder
			local newPos = this.position
			local dirFlag = false
			
			for i = 1,1000 do
				if (i % 200 == 0) then
					dirFlag = not dirFlag
				end
			
				if (dirFlag) then
					newPos.x = newPos.x + 0.01
				else
					newPos.x = newPos.x - 0.01
				end
			
				newPos.y = math.sin(i / 50)
				this.position = newPos
				
				coroutine.yield(1)
			end
		end
	))

	local triggerEveryFrame = Trigger.New()
	triggerEveryFrame:AddEvents(Event.New(EventType.EveryFrame))
	triggerEveryFrame:AddActions(Action.New(
		function(e)
			--local this = RuntimeHelper.GetSpriteObjectByID(e.binderId)
			local this = e.binder
			local newPos = this.position
			
			newPos.x = newPos.x - 0.01
			newPos.y = math.sin(CL.GetFrameCount() / 50)
			this.position = newPos
		end
	))

	local existTrigger = Trigger.New()
	existTrigger:AddEvents(Event.New(EventType.OnSpriteHpChanged, enemySprite.id))
	existTrigger:AddActions(Action.New(
		function(e)
			print("Sprite_02 is exist in frame :", CL.GetFrameCount(), " ,hp is :", e.binder.hp)
		end
	))

	local destroyingTrigger = Trigger.New()
	destroyingTrigger:AddEvents(Event.New(EventType.OnSpriteDestroying, enemySprite.id))
	destroyingTrigger:AddActions(Action.New(
		function(e)
			print("Sprite_02 is ready to die")
		end
	))

	wrapper:RegistTrigger(trigger, destroyingTrigger, existTrigger)
	
	-- 最后返回Wrapper
	return wrapper
end

return Main
