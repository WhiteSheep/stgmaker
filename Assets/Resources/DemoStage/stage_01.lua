require "StgMaker\\CLStdLib"

CL.EasyStageTrigger(1, function()

	local retValue = CL.RunScript("DemoStage\\sprite_01.lua", 1, 2)
	local wrapper = retValue[0]
	
	CL.PlaySpriteObjectWrapper(wrapper)
end)


CL.EasyStageTrigger(60, function()
	for i= 1, 3 do
		local retValue = CL.RunScript("DemoStage\\sprite_02.lua", {"aaa", "bbb"})
		local wrapper = retValue[0]
		CL.PlaySpriteObjectWrapper(wrapper)
		
		yield(120)
	end
end)
