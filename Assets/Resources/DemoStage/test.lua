local EventParam = {
	binderId
}

function EventParam:new(o)
	o = o or {}
	setmetatable(o, {__index = self})
	return o
end

function EventParam:_Init()
	self.binderId = -1
end

function EventParam:GetID()
	return self.binderId
end

function EventParam.Create()
	return EventParam:new()
end

--	------- demo -------  --

local eParam = EventParam.Create()
eParam.binderId = 33

local testFunc = function(e, ...)
	print("ID:", e:GetID())
	print("user params:", ...)
end

local params = {"aaa", "bbb"}

local co = coroutine.create(testFunc)
coroutine.resume(co, eParam , table.unpack({"aaa","bbb"}))

--	-------	Input test -------	--
local inputMappings = {
	Left = 1,
	Right = 2
}


