﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace StgMaker.Collision
{
    public class ColliderConf
    {
        //  Common Properties
        public ColliderType type;
        public Vector2 offset = Vector2.zero;
        public bool isTrigger = true;

        //  Use for BoxCollider
        public Vector2 size = new Vector2(0.5f, 0.5f);

        //  Use for CircleCollider
        public float radius = 0.5f;

        //  Use for Polygon
        public int pathCount = 1;
        public Dictionary<int, List<Vector2>> paths;

        //  Use for Edge
        public List<Vector2> edgePoints;

        public ColliderConf()
        {
            offset = Vector2.zero;
            isTrigger = true;
            size = new Vector2(0.5f, 0.5f);
            paths = new Dictionary<int, List<Vector2>>();
            edgePoints = new List<Vector2>();
        }

        public void AddPathPoints(int index, params Vector2[] prmPoints)
        {
            //  out of range
            if (index >= pathCount)
                return;

            List<Vector2> pointList;
            paths.TryGetValue(index, out pointList);

            //  check if index not exist, create new list and add to dictionary
            if (pointList == null)
            {
                pointList = new List<Vector2>();
                paths.Add(index, pointList);
            }

            //  update dictionary
            foreach (var point in prmPoints)
                pointList.Add(point);
        }

        public void AddEdgePoints(params Vector2[] prmPoints)
        {
            foreach (var point in prmPoints)
                edgePoints.Add(point);
        }
    }
}