﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.Collision
{
    public class ColliderHelper
    {
        public static void BindColliderToGameObject(GameObject obj, ColliderConf colliderConf)
        {
            switch (colliderConf.type)
            {
                case ColliderType.Circle:
                {
                    var collider = obj.AddComponent<CircleCollider2D>();
                    collider.offset = colliderConf.offset;
                    collider.radius = colliderConf.radius;
                    collider.isTrigger = colliderConf.isTrigger;
                    break;
                }
                case ColliderType.Box:
                {
                    var collider = obj.AddComponent<BoxCollider2D>();
                    collider.offset = colliderConf.offset;
                    collider.size = colliderConf.size;
                    collider.isTrigger = colliderConf.isTrigger;
                    break;
                }
                case ColliderType.Polygon:
                {
                    var collider = obj.AddComponent<PolygonCollider2D>();
                    collider.offset = colliderConf.offset;
                    collider.isTrigger = colliderConf.isTrigger;
                    collider.pathCount = colliderConf.pathCount;

                    foreach (var kv in colliderConf.paths)
                    {
                        int index = kv.Key;
                        List<Vector2> points = kv.Value;

                        collider.SetPath(index, points.ToArray());
                    }
                    break;
                }
                case ColliderType.Edge:
                {
                    var collider = obj.AddComponent<EdgeCollider2D>();
                    collider.offset = colliderConf.offset;
                    collider.isTrigger = colliderConf.isTrigger;
                    collider.points = colliderConf.edgePoints.ToArray();

                    break;
                }
                default:
                {
                    Debug.LogError("Now we don't insist this kind of type : " + colliderConf.type);
                    break;
                }
            }
        }
    }
}
