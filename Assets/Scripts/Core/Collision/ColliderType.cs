﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Collision
{
    public enum ColliderType
    {
        Circle,
        Box,
        Polygon,
        Edge,
    }
}
