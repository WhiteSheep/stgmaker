﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using UnityEngine;

namespace StgMaker.Core
{
    public class ActionHandler : IGameUpdator
    {
        private Trigger _hParent;
        private LuaFunction _luaFunction;
        private LuaTable _self;
        private string _funcName;

        private bool _finished = false;
        private int _yieldFrame = 0;
        private int _elapsedFrame = 0;

        public ActionHandler(Trigger parent, string func, LuaTable actionTable)
        {
            _hParent = parent;

            //  Create a EventArgs for action
            LuaTable eArgsTable = LuaExp.NewLuaTable();
            eArgsTable.Set("binderId", _hParent.bid);

            //  Get CLCoHelper
            LuaFunction _instantiateFunc = (LuaFunction) actionTable[func];
            object[] retVal = _instantiateFunc.Call(new object[]
            {
                actionTable,
                eArgsTable
            });
            eArgsTable.Dispose();
            _instantiateFunc.Dispose();

            //  Get Run method
            _self = (LuaTable) retVal[0];
            _luaFunction = (LuaFunction) _self["Run"];
        }

        public void Run()
        {
            // Debug.Log(string.Format("function {0} run at : frame {1}", _funcName, Time.frameCount));
            object[] retValue = _luaFunction.Call(_self);
            if (retValue == null || retValue.Length == 1)
                _finished = true;
            else
            {
                _yieldFrame = Convert.ToInt32(retValue[1]);
                _elapsedFrame = 0;
            }
        }

        public bool IsReady()
        {
            return !_finished && _elapsedFrame >= _yieldFrame;
        }

        public bool IsFinished()
        {
            return _finished;
        }

        public bool IsValid()
        {
            return !IsFinished() && _hParent.IsBinderValid();
        }

        public virtual void Update()
        {
            
        }

        public virtual void FixedUpdate()
        {
            
        }

        public virtual void LateUpdate()
        {
            _elapsedFrame += 1;
        }

        public virtual void OnDestroy()
        {
            _self.Dispose();
            _luaFunction.Dispose();
        }
    }
}
