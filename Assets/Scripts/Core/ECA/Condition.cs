﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using LuaInterface;

namespace StgMaker.Core
{
    public class Condition
    {
        private LuaFunction _condFunction;

        public Condition(LuaFunction prmFunc)
        {
            _condFunction = prmFunc;
        }

        public bool ConditionSucc()
        {
            object[] retVal = _condFunction.Call();
            if (retVal.Length == 0)
            {
                Debug.Log("condition has no return value");
                return false;
            }
            return (bool) retVal[0];
        }

        public virtual void OnDestroy()
        {
            //  Destroy condition's LuaFunction
            _condFunction.Dispose();
        }
    }
}
