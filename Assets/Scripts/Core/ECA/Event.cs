﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;

namespace StgMaker.Core
{
    public class Event : IGameUpdator
    {
        public EventType type;
        protected bool _isValid = true;

        public Event() { }

        public Event(EventType eType)
        {
            type = eType;
        }

        public virtual bool TriggerSucc(EventSignal e)
        {
            Debug.LogError("Please inherit this method!");
            return false;
        }

        public virtual bool IsValid()
        {
            return _isValid;
        }

        public virtual void Update()
        {
            
        }

        public virtual void LateUpdate()
        {
            
        }

        public virtual void FixedUpdate()
        {
            
        }

        public virtual void OnDestroy()
        {
            
        }
    }
}
