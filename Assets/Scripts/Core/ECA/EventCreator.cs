﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Debug = UnityEngine.Debug;

namespace StgMaker.Core
{
    public class EventCreator
    {
        //  Add event
        public static Event Create(EventType eventType, List<object> prm)
        {
            Event e = null;
            switch (eventType)
            {
                case EventType.EventPeriod:
                    int periodTime = Convert.ToInt32(prm[0]);
                    e = new EventPeriod(periodTime);
                    break;
                case EventType.EventOnFrameArrive:
                    int touchTime = Convert.ToInt32(prm[0]);
                    e = new EventOnFrameArrive(touchTime);
                    break;
                case EventType.EventEveryFrame:
                    e = new EventEveryFrame();
                    break;
                case EventType.EventOnKeyDown:
                    e = new EventOnKeyDown();
                    break;
                case EventType.EventOnKeyPressed:
                    e = new EventKeyOnPressed();
                    break;
                case EventType.EventOnSpriteUnderAttack:
                    int spriteId = Convert.ToInt32(prm[0]);
                    e = new EventOnSpriteUnderAttack(spriteId);
                    break;
                case EventType.EventOnSpriteHpChanged:
                    spriteId = Convert.ToInt32(prm[0]);
                    e = new EventOnSpriteHpChanged(spriteId);
                    break;
                case EventType.EventOnSpriteDestroying:
                    spriteId = Convert.ToInt32(prm[0]);
                    e = new EventOnSpriteDestroying(spriteId);
                    break;
                case EventType.EventOnSpriteDestroyed:
                    spriteId = Convert.ToInt32(prm[0]);
                    e = new EventOnSpriteDestroyed(spriteId);
                    break;
                case EventType.EventOnSpriteLifeTimeElapse:
                    int triggerTime = Convert.ToInt32(prm[0]);
                    spriteId = Convert.ToInt32(prm[1]);
                    e = new EventOnSpriteLifeTimeElapse(triggerTime, spriteId);
                    break;
                default:
                    Debug.LogError("Unsupported Event Type : " + eventType.ToString());
                    break;
            }

            return e;
        }
    }
}
