﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    /// <summary>
    /// this will be created when event occur
    /// </summary>
    public class EventSignal : Event
    {
        public bool isBind;
        public SpriteObject source;
        public EventSignal(EventType eType, SpriteObject src = null):base(eType)
        {
            if (src != null)
            {
                isBind = true;
                source = src;
            }
        }

        public override bool IsValid()
        {
            return !isBind || (source != null && !source.IfDestroyed());
        }

        public static EventSignal NoEvent = new EventSignal(EventType.EventNoEvent, null);

    }
}
