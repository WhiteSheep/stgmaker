﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class EventEveryFrame : Event
    {
        private bool _hasTriggeredInThisFrame = false;
        public override bool TriggerSucc(EventSignal e)
        {
            bool ret = false;

            //  lock so this will only run 1 time on each frame
            if (_hasTriggeredInThisFrame == false)
            {
                _hasTriggeredInThisFrame = true;
                ret = true;
            }
                
            return ret;
        }

        public override void LateUpdate()
        {
            //  unlock
            _hasTriggeredInThisFrame = false;
        }
    }
}
