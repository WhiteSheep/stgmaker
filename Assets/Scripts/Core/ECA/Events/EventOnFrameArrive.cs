﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.Core
{
    public class EventOnFrameArrive : Event
    {
        private int _touchFrame;
        
        public EventOnFrameArrive(int frame):base(EventType.EventOnFrameArrive)
        {
            _touchFrame = frame;
        }

        public override bool TriggerSucc(EventSignal e)
        {
            if (Time.frameCount > _touchFrame)
                _isValid = false;

            return _touchFrame == Time.frameCount;
        }

        public override void Update()
        {
            
        }

        public override bool IsValid()
        {
            return _isValid;
        }
    }
}
