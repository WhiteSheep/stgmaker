﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class EventOnKeyDown : Event
    {
        public override bool TriggerSucc(EventSignal e)
        {
            return e.type == EventType.EventOnKeyDown;
        }
    }
}
