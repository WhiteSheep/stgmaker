﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class EventOnSpriteDestroying : Event
    {
        private int _spriteId;

        public EventOnSpriteDestroying(int prmId)
        {
            _spriteId = prmId;
        }

        public override bool TriggerSucc(EventSignal e)
        {
            return e.type == EventType.EventOnSpriteDestroying
                   && e.source != null
                   && e.source.uid == _spriteId;
        }

        public override bool IsValid()
        {
            return Locator.GetSpriteMgr().GetSpriteByUID(_spriteId) != null;
        }
    }
}
