﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class EventOnSpriteHpChanged : Event
    {
        private int _id;

        public EventOnSpriteHpChanged(int prmId)
        {
            _id = prmId;
        }

        public override bool TriggerSucc(EventSignal e)
        {
            return e.type == EventType.EventOnSpriteHpChanged 
                && e.source != null 
                && e.source.uid == _id;
        }

        public override bool IsValid()
        {
            return Locator.GetSpriteMgr().GetSpriteByUID(_id) != null;
        }
    }
}
