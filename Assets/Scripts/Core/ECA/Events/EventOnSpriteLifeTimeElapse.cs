﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class EventOnSpriteLifeTimeElapse : Event
    {
        private int _triggerFrame;
        private SpriteObject _binder;

        public EventOnSpriteLifeTimeElapse(int prmFrame, int prmSpriteId)
        {
            _triggerFrame = prmFrame;
            _binder = Locator.GetSpriteMgr().GetSpriteByUID(prmSpriteId);
        }

        public override bool TriggerSucc(EventSignal e)
        {
            return _binder != null && !_binder.IfDestroyed() && _binder.ElapsedFrame == _triggerFrame;
        }

        public override bool IsValid()
        {
            return _binder != null && !_binder.IfDestroyed() && _binder.ElapsedFrame <= _triggerFrame;
        }
    }
}
