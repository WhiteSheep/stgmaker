﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class EventOnSpriteUnderAttack : Event
    {
        private int _binderId;

        public EventOnSpriteUnderAttack(int prmBinderId)
        {
            _binderId = prmBinderId;
        }

        public override bool TriggerSucc(EventSignal e)
        {
            return (
                e.type == EventType.EventOnSpriteUnderAttack
                && e.source != null
                && e.source.uid == _binderId
                );
        }

        public override bool IsValid()
        {
            return Locator.GetSpriteMgr().GetSpriteByUID(_binderId) != null;
        }
    }
}