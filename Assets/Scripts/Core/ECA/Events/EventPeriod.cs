﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.Core
{
    class EventPeriod : Event
    {
        private int _period;
        private int _elapsed = 0;

        public EventPeriod(int time)
            : base(EventType.EventPeriod)
        {
            _period = time;
        }

        public override bool TriggerSucc(EventSignal e)
        {
            if (_elapsed >= _period)
            {
                _elapsed = 0;
                return true;
            }

            return false;
        }

        public override void LateUpdate()
        {
            _elapsed += 1;
        }
        

    }
}
