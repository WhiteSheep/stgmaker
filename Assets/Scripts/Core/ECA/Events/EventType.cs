﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public enum EventType
    {
        EventNoEvent = -1,
        EventPeriod,
        EventOnFrameArrive,
        EventEveryFrame,
        EventSpriteInitialization,
        EventOnKeyDown,
        EventOnKeyPressed,
        EventOnSpriteUnderAttack,
        EventOnSpriteHpChanged,
        EventOnSpriteDestroying,
        EventOnSpriteDestroyed,
        EventOnSpriteLifeTimeElapse,
    }
}
