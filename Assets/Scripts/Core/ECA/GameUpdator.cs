﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;

namespace StgMaker.Core
{
    public interface IGameUpdator
    {
        void Update();
        void FixedUpdate();
        void LateUpdate();
        void OnDestroy();
    }
}