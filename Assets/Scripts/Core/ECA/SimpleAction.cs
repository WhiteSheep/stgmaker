﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using UnityEngine;

namespace StgMaker.Core
{
    public class SimpleAction
    {
        private string _functionName;
        private LuaTable _luaTable;

        public SimpleAction() { }
        public SimpleAction(LuaTable luaTable, string functionName)
        {
            _luaTable = luaTable;
            _functionName = functionName;
        }

        public ActionHandler CreateHandler(Trigger parent)
        {
            return new ActionHandler(parent, _functionName, _luaTable);
        }

        public void OnDestroy()
        {
            //  Destroy action's LuaTable
            _luaTable.Dispose();
        }
    }
}
