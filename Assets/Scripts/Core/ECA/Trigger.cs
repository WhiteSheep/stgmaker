﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class Trigger : SEFBaseClass, IGameUpdator
    {
        private List<Event> _events;
        private Condition _condition;
        private List<SimpleAction> _actions;
        private bool _isValid = true;

        //  Binder uid
        public int bid = -1;
        public SpriteObject binder = null;
        public bool ifBind = false;
        

        public Trigger()
        {
            _events = new List<Event>();
            _actions = new List<SimpleAction>();
        }

        public bool IsValid()
        {
            return _isValid;
        }

        public bool IsBinderValid()
        {
            return (!ifBind) || (binder != null && !binder.IfDestroyed());
        }

        public void AddEvent(Event e)
        {
            _events.Add(e);
        }

        public void AddCondition(Condition cond)
        {
            _condition = cond;
        }

        public void AddAction(SimpleAction action)
        {
            _actions.Add(action);
        }

        public virtual void Update()
        {
            if (_isValid)
            {
                //  _events is valid if any event valid
                bool eventValid = false;
                foreach (Event e in _events)
                {
                    eventValid = e.IsValid();
                    if (eventValid)
                        break;
                }

                //  no binder or binder is exist, and event must be valid
                _isValid = ((binder != null && !binder.IfDestroyed()) || !ifBind)
                    && eventValid;
            }
        }

        public virtual void FixedUpdate()
        {
            
        }

        public virtual void LateUpdate()
        {
            //  refresh data in events
            foreach (Event e in _events)
                e.LateUpdate();
        }

        public virtual void OnDestroy()
        {
            _condition.OnDestroy();

            foreach (SimpleAction action in _actions)
            {
                action.OnDestroy();
            }
        }

        public void Notice(EventSignal signal)
        {
            foreach (Event e in _events)
            {
                if (e.TriggerSucc(signal))
                {
                    if (_condition.ConditionSucc())
                        foreach (SimpleAction action in _actions)
                        {
                            ActionHandler actHdl = action.CreateHandler(this);
                            Locator.GetActionHandlerMgr().AddActionHandler(actHdl);
                        }
                }
            }
        }
    }
}
