﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mime;
using System.Text;
using UnityEngine;

namespace StgMaker.Core
{
    public enum EnvItem
    {
        ProjectPath = 0
    }

    public static class Env
    {
        private static string _projectPath = "";

        public static void Regist(EnvItem envItem, string value)
        {
            switch (envItem)
            {
                case EnvItem.ProjectPath:
                    _projectPath = Application.dataPath + "/Resources/" + value;
                    break;
            }
        }

        public static string Get(EnvItem envItem)
        {
            switch (envItem)
            {
                case EnvItem.ProjectPath:
                    return _projectPath;
                default:
                    return null;
            }
        }
    }
}
