﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.InputModule
{
    public enum BuildInVirtualButton
    {
        Up = 0,
        Down,
        Left,
        Right,
        Attack,
        Slow,
        Bomb,
        Confirm,
        Empty0,
        Empty1,
        Empty2,
        Empty3,
        Empty4,
        Empty5,
        Empty6,
        Empty7,
        Empty8,
        Empty9
    }
}