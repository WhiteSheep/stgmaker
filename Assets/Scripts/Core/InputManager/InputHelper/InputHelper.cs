﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.InputModule
{
    public class InputHelper
    {
        public static string GetVirtualButtonName(BuildInVirtualButton prmValue)
        {
            return Enum.GetName(typeof (BuildInVirtualButton), prmValue);
        }
    }
}
