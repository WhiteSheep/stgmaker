﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.InputModule
{
    public class StandaloneInput : VirtualInput
    {
        public override bool GetButton(string prmName)
        {
            VirtualButton button = GetVirtualButton(prmName);

            if (button != null)
                return GetKey(button.positiveButton);

            return false;
        }

        //-----------------------------------------------------------------------
        public override bool GetButtonDown(string prmName)
        {
            VirtualButton button = GetVirtualButton(prmName);

            if (button != null)
                return GetKeyDown(button.positiveButton);

            return false;
        }

        //-----------------------------------------------------------------------
        public override bool GetButtonUp(string prmName)
        {
            VirtualButton button = GetVirtualButton(prmName);

            if (button != null)
                return GetKeyUp(button.positiveButton);

            return false;
        }

        //-----------------------------------------------------------------------
        public override bool GetKey(KeyCode prmKeyCode)
        {
            return Input.GetKey(prmKeyCode);
        }

        //-----------------------------------------------------------------------
        public override bool GetKeyDown(KeyCode prmKeyCode)
        {
            return Input.GetKeyDown(prmKeyCode);
        }

        //-----------------------------------------------------------------------
        public override bool GetKeyUp(KeyCode prmKeyCode)
        {
            return Input.GetKeyUp(prmKeyCode);
        }
    }
}