﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StgMaker.Core;
using UnityEngine;

namespace StgMaker.InputModule
{
    public static class InputManager
    {
        private static VirtualInput _standaloneInput;
        private static VirtualInput _mobileInput;
        private static VirtualInput _activeInput;

        public enum InputPlatform
        {
            Standalone,
            Mobile,
            JoyStick
        }

        static InputManager()
        {
            _standaloneInput = new StandaloneInput();

            //  In demo, we select StandaloneHelper to default input
            _activeInput = _standaloneInput;

            //  System will bind VirtualButton for default
            DefaultVirtualButtonMappingInit();
        }

        public static void DefaultVirtualButtonMappingInit()
        {
            VirtualButton virtualBtn;
 
            virtualBtn = RegistVirtualButton(InputHelper.GetVirtualButtonName(BuildInVirtualButton.Up));
            virtualBtn.positiveButton = KeyCode.UpArrow;

            virtualBtn = RegistVirtualButton(InputHelper.GetVirtualButtonName(BuildInVirtualButton.Down));
            virtualBtn.positiveButton = KeyCode.DownArrow;

            virtualBtn = RegistVirtualButton(InputHelper.GetVirtualButtonName(BuildInVirtualButton.Left));
            virtualBtn.positiveButton = KeyCode.LeftArrow;

            virtualBtn = RegistVirtualButton(InputHelper.GetVirtualButtonName(BuildInVirtualButton.Right));
            virtualBtn.positiveButton = KeyCode.RightArrow;
        }

        //-----------------------------------------------------------------------
        public static bool AnyKeyDown()
        {
            return Input.anyKeyDown;
        }

        //-----------------------------------------------------------------------
        public static bool AnyKey()
        {
            return Input.anyKey;
        }

        //-----------------------------------------------------------------------
        public static bool ButtonExist(string prmButton)
        {
            return _activeInput.ButtonExist(prmButton);
        }

        //-----------------------------------------------------------------------
        public static VirtualButton RegistVirtualButton(string prmName)
        {
            return _activeInput.RegistVirtualButton(prmName);
        }

        //-----------------------------------------------------------------------
        public static void UnRegistVirtualButton(string prmName)
        {
            _activeInput.UnRegistVirtualButton(prmName);
        }

        //-----------------------------------------------------------------------
        public static bool GetKeyDown(KeyCode prmKeyCode)
        {
            return _activeInput.GetKeyDown(prmKeyCode);
        }

        //-----------------------------------------------------------------------
        public static bool GetKeyUp(KeyCode prmKeyCode)
        {
            return _activeInput.GetKeyUp(prmKeyCode);
        }

        //-----------------------------------------------------------------------
        public static bool GetKey(KeyCode prmKeyCode)
        {
            return _activeInput.GetKey(prmKeyCode);
        }

        //-----------------------------------------------------------------------
        public static bool GetButtonDown(string prmButton)
        {
            return _activeInput.GetButtonDown(prmButton);
        }

        //-----------------------------------------------------------------------
        public static bool GetButtonUp(string prmButton)
        {
            return _activeInput.GetButtonUp(prmButton);
        }

        //-----------------------------------------------------------------------
        public static bool GetButton(string prmButton)
        {
            return _activeInput.GetButton(prmButton);
        }
    }
}