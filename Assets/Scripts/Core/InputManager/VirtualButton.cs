﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.InputModule
{
    public class VirtualButton
    {
        public VirtualButton(string prmName)
        {
            name = prmName;
        }

        public string name { get; private set; }
        public KeyCode positiveButton;
        public KeyCode negativeButton;
        public bool invert;
        public bool dead;
    }
}
