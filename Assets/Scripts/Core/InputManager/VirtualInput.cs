﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.InputModule
{
    public abstract class VirtualInput
    {
        private Dictionary<string, VirtualButton> _registVirtualButtons = new Dictionary<string, VirtualButton>();

        public VirtualButton RegistVirtualButton(string prmName)
        {
            VirtualButton button;
            if (_registVirtualButtons.TryGetValue(prmName, out button))
            {
                return button;
            }

            button = new VirtualButton(prmName);
            _registVirtualButtons.Add(prmName, button);

            return button;
        }

        //-----------------------------------------------------------------------
        public void UnRegistVirtualButton(string prmName)
        {
            _registVirtualButtons.Remove(prmName);
        }

        //-----------------------------------------------------------------------
        public bool ButtonExist(string prmButton)
        {
            return _registVirtualButtons.ContainsKey(prmButton);
        }

        //-----------------------------------------------------------------------
        protected VirtualButton GetVirtualButton(string prmButton)
        {
            VirtualButton retButton;
            _registVirtualButtons.TryGetValue(prmButton, out retButton);

            return retButton;
        }

        //-----------------------------------------------------------------------
        public abstract bool GetKey(KeyCode prmKeyCode);
        //-----------------------------------------------------------------------
        public abstract bool GetKeyDown(KeyCode prmKeyCode);
        //-----------------------------------------------------------------------
        public abstract bool GetKeyUp(KeyCode prmKeyCode);
        //-----------------------------------------------------------------------
        public abstract bool GetButton(string prmName);
        //-----------------------------------------------------------------------
        public abstract bool GetButtonDown(string prmName);
        //-----------------------------------------------------------------------
        public abstract bool GetButtonUp(string prmName);
    }
}