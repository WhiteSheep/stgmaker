﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Core;
using UnityEngine;

namespace StgMaker.Core
{
    public class Locator
    {
        private static SEFTriggerMgr _triggerMgr = null;
        private static SEFActionHandlerMgr _actionHdlMgr = null;
        private static SEFSpriteMgr _spriteMgr = null;
        private static SEFResourceMgr _resourceMgr = null;
        private static SEFAnimationMgr _animationMgr = null;

        public static void Regist(SEFTriggerMgr val)
        {
            _triggerMgr = val;
        }

        public static void Regist(SEFActionHandlerMgr actHdlMgr)
        {
            _actionHdlMgr = actHdlMgr;
        }

        public static void Regist(SEFSpriteMgr spriteMgr)
        {
            _spriteMgr = spriteMgr;
        }

        public static void Regist(SEFResourceMgr resource)
        {
            _resourceMgr = resource;
        }

        public static void Regist(SEFAnimationMgr animationMgr)
        {
            _animationMgr = animationMgr;
        }

        public static SEFTriggerMgr GetTriggerMgr()
        {
            if (_triggerMgr == null)
                _triggerMgr = new SEFTriggerMgrDefault();

            return _triggerMgr;
        }

        public static SEFActionHandlerMgr GetActionHandlerMgr()
        {
            if (_actionHdlMgr == null)
                _actionHdlMgr = new SEFActionHandlerMgrDefault();

            return _actionHdlMgr;
        }

        public static SEFSpriteMgr GetSpriteMgr()
        {
            if (_spriteMgr == null)
                _spriteMgr = new SEFSpriteMgrDefault();

            return _spriteMgr;
        }

        public static SEFResourceMgr GetResourceMgr()
        {
            if (_resourceMgr == null)
                _resourceMgr = new SEFResourceMgrDefault();

            return _resourceMgr;
        }

        public static SEFAnimationMgr GetAnimationMgr()
        {
            if (_animationMgr == null)
                _animationMgr = new SAnimationSystem();

            return _animationMgr;
        }

        public static void UnSetup()
        {
            _triggerMgr = null;
            _spriteMgr = null;
            _actionHdlMgr = null;
            _resourceMgr = null;
            _animationMgr = null;
        }
    }
}
