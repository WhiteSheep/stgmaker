﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class SAnimationBinder
    {
        public SAnimationController animCtrl;
        public SpriteObject spriteObject;

        public SAnimationBinder() { }

        public SAnimationBinder(SAnimationController prmCtrl, SpriteObject prmObj)
        {
            animCtrl = prmCtrl;
            spriteObject = prmObj;
        }
    }
}
