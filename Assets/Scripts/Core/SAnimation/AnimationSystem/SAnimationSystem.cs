﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using UnityEngine;

namespace StgMaker.Core
{
    public class SAnimationSystem : SEFAnimationMgr
    {
        private List<SAnimationBinder> _binders;

        public SAnimationSystem()
        {
            _binders = new List<SAnimationBinder>();
        }

        public override void RegistController(SAnimationController prmCtrl, SpriteObject prmObj)
        {
            SAnimationBinder binder = new SAnimationBinder(prmCtrl, prmObj);
            _binders.Add(binder);
        }

        public override void Update()
        {
            foreach (var binder in _binders)
            {
                if (!binder.spriteObject.IfDestroyed())
                {
                    SpriteRenderer renderer = binder.spriteObject.GetSpriteRenderer();

                    if (renderer != null)
                        renderer.sprite = binder.animCtrl.Update();
                }
            }
        }

        public override void LateUpdate()
        {
            List<SAnimationBinder> removedBinders = new List<SAnimationBinder>();

            //  check need removed binders, and do lateupdate
            foreach (var binder in _binders)
            {
                if (binder.spriteObject.IfDestroyed())
                {
                    removedBinders.Add(binder);
                }
                else
                {
                    binder.animCtrl.LateUpdate();    
                }
            }

            //  remove
            foreach (var binder in removedBinders)
                _binders.Remove(binder);

        }

        public override void FixedUpdate()
        {
            
        }
    }
}
