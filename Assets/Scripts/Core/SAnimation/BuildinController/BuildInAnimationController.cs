﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using LuaInterface;
using UnityEngine;

namespace StgMaker.Core
{
    public class BuildInAnimationController
    {
        public static SAnimationController Create(LuaTable animationGroup)
        {
            SAnimationController animCtrl = null;

            var animCtrlType = (BuildInAnimationControllerType)
                Convert.ToInt32(animationGroup["animationControllerType"]);
            switch (animCtrlType)
            {
                case BuildInAnimationControllerType.SimpleSprite:
                    animCtrl = BuildInAnimationLib.SimpleSpriteAnimationController();
                    break;
                case BuildInAnimationControllerType.SpriteEffect:
                    animCtrl = BuildInAnimationLib.SpriteEffectAnimationController();
                    break;
            }

            //  Load all clips
            LuaTable animationClips = (LuaTable)animationGroup["animationClips"];
            IDictionaryEnumerator cItor = animationClips.GetEnumerator();

            while (cItor.MoveNext())
            {
                LuaTable clipTable = (LuaTable) cItor.Value;
                var animStateType = (BuildInAnimationStateType) Convert.ToInt32(clipTable["animStateType"]);
                SAnimationClip animClip = null;

                //  Analysis clip type
                LuaTable clip = (LuaTable)clipTable["clip"];
                var clipType = (SAnimationClipType) Convert.ToInt32(clip["clipType"]);
                switch (clipType)
                {
                    case SAnimationClipType.Grid:
                    {
                        int gridWidht = Convert.ToInt32(clip["gridWidth"]);
                        int gridHeight = Convert.ToInt32(clip["gridHeight"]);
                        string textureId = (string) clip["textureId"];
                        int startPos = Convert.ToInt32(clip["startPos"]);
                        int length = Convert.ToInt32(clip["length"]);
                        int frameCount = Convert.ToInt32(clip["frameCount"]);

                        Texture2D tex2D = Locator.GetResourceMgr().Load<Texture2D>(textureId);
                        SSpriteSheet spriteSheet = new SSpriteSheet();
                        spriteSheet = spriteSheet.Slice(tex2D, gridWidht, gridHeight).SubSprites(startPos, length);
                        animClip = new SAnimationClip(spriteSheet, frameCount);

                        break;
                    }
                    case SAnimationClipType.CustomRect:
                    {
                        //  first get basic info
                        string textureId = (string) clip["textureId"];
                        int frameCount = Convert.ToInt32(clip["frameCount"]);

                        Texture2D tex2D = Locator.GetResourceMgr().Load<Texture2D>(textureId);
                        SSpriteSheet spriteSheet = new SSpriteSheet();

                        //  iterator all rect in rects
                        LuaTable rects = (LuaTable)clip["customRects"];
                        IDictionaryEnumerator rItor = rects.GetEnumerator();

                        while (rItor.MoveNext())
                        {
                            LuaTable singleRect = (LuaTable) rItor.Value;
                            Vector2 startPos = new Vector2(Convert.ToInt32(singleRect["x"]), Convert.ToInt32(singleRect["y"]));
                            Vector2 rectSize = new Vector2(Convert.ToInt32(singleRect["w"]), Convert.ToInt32(singleRect["h"]));

                            SSliceDirection sliceDirection = (SSliceDirection) Convert.ToInt32(singleRect["direction"]);
                            int length = Convert.ToInt32(singleRect["length"]);

                            spriteSheet.Slice(tex2D, startPos, rectSize, sliceDirection, length);
                        }

                        //  Create AnimationClip
                        animClip = new SAnimationClip(spriteSheet, frameCount);
                        break;
                    }
                }

                //  Bind SAnimationClip to SAnimationController
                SAnimationState state = animCtrl.GetState(_GetBuildInStateName(animStateType));
                if (state != null)
                    state.motion = animClip;
            }

            return animCtrl;
        }

        private static string _GetBuildInStateName(BuildInAnimationStateType type)
        {
            string retName = "";
            switch (type)
            {
                case BuildInAnimationStateType.Idle: retName = "Front"; break;
                case BuildInAnimationStateType.Left: retName = "Left"; break;
                case BuildInAnimationStateType.ToLeft: retName = "ToLeft"; break;
                case BuildInAnimationStateType.ToRight: retName = "ToRight"; break;
                case BuildInAnimationStateType.Right: retName = "Right"; break;
                default: Debug.LogError("Unknown build in state name"); break;
            }

            return retName;
        }

        
    }
}
