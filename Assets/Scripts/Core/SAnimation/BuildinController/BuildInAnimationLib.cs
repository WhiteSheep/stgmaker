﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.Core
{
    public class BuildInAnimationLib
    {
        public static SAnimationController SimpleSpriteAnimationController()
        {
            SAnimationController controller = new SAnimationController();

            //  Regist variable
            Variable isOverVar = new Variable("IsOver", false);
            Variable acceleration = new Variable("Acceleration", 0.0f);

            controller.AddVar(new[] { acceleration });

            //  Create all state
            SAnimationState frontState = new SAnimationState("Front");
            SAnimationState toleftState = new SAnimationState("ToLeft").ForcePlayOnce(true);
            SAnimationState leftState = new SAnimationState("Left");
            SAnimationState torightState = new SAnimationState("ToRight").ForcePlayOnce(true);
            SAnimationState rightState = new SAnimationState("Right");

            // -----------  Set state info  ------------

            //  front
            frontState.motion = null;

            STransition transition = new STransition(controller.ExitState);
            transition.AddCondition(new SConditionBool("IsOver", true));
            frontState.AddTransition(transition);

            STransition idle_toLeft = new STransition(toleftState);
            idle_toLeft.AddCondition(new SConditionFloat("Acceleration", 0.0f, SConditionCompareType.Less));
            frontState.AddTransition(idle_toLeft);

            STransition idle_toRight = new STransition(torightState);
            idle_toRight.AddCondition(new SConditionFloat("Acceleration", 0.0f, SConditionCompareType.Greater));
            frontState.AddTransition(idle_toRight);

            //  toleft
            toleftState.motion = null;
            toleftState.AddTransition(new STransition(leftState));

            //  left
            leftState.motion = null;
            STransition trans_front = new STransition(frontState);
            trans_front.AddCondition(new SConditionFloat("Acceleration", 0.0f, SConditionCompareType.GreaterOrEqual));
            leftState.AddTransition(trans_front);

            //  toright
            torightState.motion = null;
            torightState.AddTransition(new STransition(rightState));

            //  rightstate
            rightState.motion = null;
            STransition right_to_idle = new STransition(frontState);
            right_to_idle.AddCondition(new SConditionFloat("Acceleration", 0.0f, SConditionCompareType.LessOrEqual));
            rightState.AddTransition(right_to_idle);

            //  finally
            controller.AddState(frontState);
            controller.AddState(toleftState);
            controller.AddState(leftState);
            controller.AddState(torightState);
            controller.AddState(rightState);
            controller.SetEntryState(frontState);

            return controller;
        }

        public static SAnimationController SpriteEffectAnimationController()
        {
            SAnimationController spriteEffectAC = new SAnimationController();
            
            //  Create SAnimation state
            SAnimationState effectState = new SAnimationState("effect");
            effectState.ForcePlayOnce(true);

            //  Create Transitions
            STransition effect_to_exit = new STransition(spriteEffectAC.ExitState);
            effectState.AddTransition(effect_to_exit);

            //  Add 
            spriteEffectAC.AddState(effectState);
            spriteEffectAC.SetEntryState(effectState);

            return spriteEffectAC;
        }
    }
}