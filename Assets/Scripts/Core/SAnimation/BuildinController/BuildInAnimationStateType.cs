﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public enum BuildInAnimationStateType
    {
        Idle,
        ToLeft,
        Left,
        ToRight,
        Right,
        Effect,
    }
}
