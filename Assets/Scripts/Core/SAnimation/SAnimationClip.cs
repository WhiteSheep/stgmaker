﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace StgMaker.Core
{
    public class SAnimationClip
    {
        public readonly SSpriteSheet spriteSheet;
        public readonly int fps;
        public readonly int fullFrameCount;

        public SAnimationClip(SSpriteSheet ss, int animFps = 0)
        {
            //  calculate best fps
            animFps = (animFps == 0) ? ss.Count() * 5 : animFps;

            spriteSheet = ss;
            fps = animFps/ss.Count();
            fullFrameCount = spriteSheet.Count() * fps;
        }
    }
}
