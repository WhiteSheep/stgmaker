﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.Core
{
    public class SAnimationController
    {
        public readonly SAnimationState EntryState;
        public readonly SAnimationState ExitState;
        public string name;

        private VarPool _varPool = new VarPool();
        private Dictionary<string, SAnimationState> _states;
        private SAnimationState _nowRunningState;

        public SAnimationController(string ctrlName = "DefaultAnimCtrl")
        {
            name = ctrlName;
            _states = new Dictionary<string, SAnimationState>();

            EntryState = new SAnimationState("Entry");
            ExitState = new SAnimationState("Exit");

            Reset();
        }

        public void AddVar(Variable var)
        {
            _varPool.Add(var);
        }

        public void AddVar(Variable[] vars)
        {
            foreach (Variable var in vars)
                AddVar(var);
        }

        public void RemoveVar(Variable var)
        {
            RemoveVar(var.name);
        }

        public void RemoveVar(string varname)
        {
            _varPool.Remove(varname);
        }

        public void SetVarValue(string varname, object value)
        {
            /*
            Debug.Log(string.Format("AnimationController - {0} : Set *{1}* --> {2}"
                ,name, varname, value));*/
            _varPool.SetVarValue(varname, value);
        }

        public void AddState(SAnimationState state)
        {
            _states.Add(state.name, state);
        }

        public SAnimationState GetState(string stateName)
        {
            SAnimationState retState;
            _states.TryGetValue(stateName, out retState);

            return retState;
        }

        public void RemoveState(string stateName)
        {
            _states.Remove(stateName);
        }

        public void Reset()
        {
            _nowRunningState = EntryState;
        }

        public void SetEntryState(SAnimationState entry)
        {
            EntryState.ClearAllTransition();

            STransition transition = new STransition(entry);
            EntryState.AddTransition(transition);
        }

        public Sprite Update()
        {
            SAnimationState newState = _nowRunningState.Update(_varPool);

            //  state change
            if (newState != _nowRunningState)
            {
                Debug.Log(string.Format("Stage change : {0} ---> {1}", _nowRunningState.name, newState.name));
                _nowRunningState = newState;
                _nowRunningState.Reset();
            }

            return _nowRunningState.GetSprite();
        }

        public void LateUpdate()
        {
            _varPool.Refresh();
        }

        public bool IsFinish()
        {
            return _nowRunningState == ExitState;
        }
    }
}
