﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.Core
{
    public static class SAnimationHelper
    {
        public static Vector2 GetPovit(SPivotMode mode)
        {
            switch (mode)
            {
                case SPivotMode.Bottom:
                    return new Vector2(0.5f, 0f);
                case SPivotMode.BottomLeft:
                    return new Vector2(0f, 0f);
                case SPivotMode.BottomRight:
                    return new Vector2(1f, 0f);
                case SPivotMode.Center:
                    return new Vector2(0.5f, 0.5f);
                case SPivotMode.Left:
                    return new Vector2(0f, 0.5f);
                case SPivotMode.Right:
                    return new Vector2(1f, 0.5f);
                case SPivotMode.Top:
                    return new Vector2(0.5f, 1f);
                case SPivotMode.TopLeft:
                    return new Vector2(0f, 1f);
                case SPivotMode.TopRight:
                    return new Vector2(1f, 1f);
                default:
                    return new Vector2(0.5f, 0.5f);
            }
        }
    }
}
