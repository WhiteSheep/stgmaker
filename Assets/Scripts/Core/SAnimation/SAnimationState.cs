﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace StgMaker.Core
{
    public class SAnimationState
    {
        public string name;
        public SAnimationClip motion;
        public float speed = 1f;
        public bool mirror;
        public bool loop = true;

        private Dictionary<string, STransition> _transitions;
        private float _elapsedTime;
        private bool _finishedOnce;
        private bool _forcePlayOnce;

        public SAnimationState(string stateName)
        {
            name = stateName;
            _transitions = new Dictionary<string, STransition>();
        }

        public void AddTransition(STransition trans)
        {
            _transitions.Add(trans.targetState.name, trans);
        }

        public void RemoveTransition(string stateName)
        {
            _transitions.Remove(stateName);
        }

        public void ClearAllTransition()
        {
            _transitions.Clear();
        }

        public SAnimationState Update(VarPool pool)
        {
            SAnimationState nextState = this;

            //  this check will force a animation run at least once
            if (!_forcePlayOnce || _finishedOnce)
            {
                foreach (var kv in _transitions)
                {
                    STransition trans = kv.Value;
                    if (trans.NeedTransition(pool))
                    {
                        nextState = trans.targetState;
                        break;
                    }
                }    
            }
            
            return nextState;
        }

        public Sprite GetSprite()
        {
            Sprite retSprite = null;

            if (motion != null)
            {
                //  calculate frameIdx
                _elapsedTime += speed;
                _finishedOnce = (_elapsedTime + speed) >= motion.fullFrameCount;

                if (loop)
                    _elapsedTime = _elapsedTime % motion.fullFrameCount;
                else
                    _elapsedTime = _elapsedTime < motion.fullFrameCount ? _elapsedTime : motion.fullFrameCount;

                int frameIdx = (int)Math.Floor(_elapsedTime / motion.fps);
                frameIdx = (frameIdx < motion.spriteSheet.Count()) ? frameIdx : motion.spriteSheet.Count() - 1;
                
                //Debug.Log(string.Format("{0} : sprite idx = {1}", Time.frameCount, frameIdx));

                retSprite = motion.spriteSheet[frameIdx];
            }

            return retSprite;
        }

        public void Reset()
        {
            _elapsedTime = 0f;
        }

        public SAnimationState ForcePlayOnce(bool flagValue)
        {
            _forcePlayOnce = flagValue;
            return this;
        }

    }
}
