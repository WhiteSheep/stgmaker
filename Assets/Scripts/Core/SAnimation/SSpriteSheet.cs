﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace StgMaker.Core
{
    public class SSpriteSheet
    {
        public List<Sprite> sprites = new List<Sprite>();
        public Vector2 pivot;

        public SSpriteSheet()
        {
        }

        public SSpriteSheet Slice(Texture2D tex2d, Vector2 startPos, Vector2 rectSize, SSliceDirection direction,
            int length, SPivotMode mode = SPivotMode.Center, Vector2 customPivot = default(Vector2))
        {
            if (mode != SPivotMode.Custom)
                pivot = SAnimationHelper.GetPovit(mode);
            else
                pivot = customPivot;

            //  Slice all sprites
            for (int idx = 0; idx < length; ++idx)
            {
                Rect rect;
                if (direction == SSliceDirection.Horizonal)
                    rect = new Rect(startPos + new Vector2(rectSize.x*idx, 0), rectSize);
                else
                    rect = new Rect(startPos - new Vector2(0, rectSize.y*idx), rectSize);

                Sprite sprite = Sprite.Create(tex2d, rect, pivot);
                sprites.Add(sprite);
            }

            return this;
        }

        public SSpriteSheet Slice(Texture2D tex2d, int cellWidth, int cellHeight, int count = 0,
            SPivotMode mode = SPivotMode.Center, Vector2 customPivot = default(Vector2))
        {
            ClearAll();

            //  Set pivot
            if (mode != SPivotMode.Custom)
                pivot = SAnimationHelper.GetPovit(mode);
            else
                pivot = customPivot;

            //  Slice all sprite
            int loadedCount = 0;
            for (int y = tex2d.height - cellHeight; y >= 0; y -= cellHeight)
                for (int x = 0; x < tex2d.width; x += cellWidth)
                {
                    Rect rect = new Rect(
                        new Vector2(x, y),
                        new Vector2(cellWidth, cellHeight));

                    Sprite sprite = Sprite.Create(tex2d, rect, pivot);
                    sprites.Add(sprite);

                    ++loadedCount;
                    if (count > 0 && loadedCount >= count)
                        break;
                }

            return this;
        }

        public SSpriteSheet SubSprites(int startPos, int count)
        {
            SSpriteSheet spriteSheet = new SSpriteSheet();

            spriteSheet.sprites = sprites.GetRange(startPos, count);
            spriteSheet.pivot = pivot;

            return spriteSheet;
        }

        public SSpriteSheet ClearAll()
        {
            sprites.Clear();
            return this;
        }

        public int Count()
        {
            return sprites.Count;
        }

        public Sprite this[int key]
        {
            get { return sprites[key]; }
        }
    }
}