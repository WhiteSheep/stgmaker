﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StgMaker.Core;

namespace StgMaker.Core
{
    class SConditionInt : SCondition
    {
        private string _name;
        private int _value;
        private SConditionCompareType _type;

        public SConditionInt() {}

        public SConditionInt(string varName, int varValue, SConditionCompareType cmpType)
        {
            _name = varName;
            _value = varValue;
            _type = cmpType;
        }

        public override bool Calculate(VarPool vars)
        {
            int value = vars.GetVarValue<int>(_name);
            return SConditionCompare.Compare(value, _value, _type);
        }
    }
}
