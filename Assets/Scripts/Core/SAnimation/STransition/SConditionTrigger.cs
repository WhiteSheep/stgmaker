﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class SConditionTrigger : SCondition
    {
        private string _name;

        public SConditionTrigger(string triggerName)
        {
            _name = triggerName;
        }

        public override bool Calculate(VarPool vars)
        {
            Variable var = vars.GetVariable(_name);

            if (var == null)
                return false;

            return var.isTrigger && (bool) var.value;
        }
    }
}
