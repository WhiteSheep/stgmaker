﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class SConditionBool : SCondition
    {
        private string _boolName;
        private bool _expectValue;

        public SConditionBool(string boolValueName, bool expectValue)
        {
            _boolName = boolValueName;
            _expectValue = expectValue;
        }

        public override bool Calculate(VarPool vars)
        {
            return vars.GetVarValue<bool>(_boolName) == _expectValue;
        }
    }
}
