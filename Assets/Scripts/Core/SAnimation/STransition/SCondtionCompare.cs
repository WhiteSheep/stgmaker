﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public enum SConditionCompareType
    {
        Equal,
        NotEqual,
        Greater,
        GreaterOrEqual,
        Less,
        LessOrEqual
    }

    public class SConditionCompare
    {
        public static bool Compare<T>(T numA, T numB, SConditionCompareType cmpType)
        {
            IComparable comparable = (IComparable) numA;
            switch (cmpType)
            {
                case SConditionCompareType.Equal:
                    return comparable != null && comparable.CompareTo(numB) == 0;
                case SConditionCompareType.NotEqual:
                    return comparable != null && comparable.CompareTo(numB) != 0;
                case SConditionCompareType.Greater:
                    return comparable != null && comparable.CompareTo(numB) > 0;
                case SConditionCompareType.GreaterOrEqual:
                    return comparable != null && comparable.CompareTo(numB) >= 0;
                case SConditionCompareType.Less:
                    return comparable != null && comparable.CompareTo(numB) < 0;
                case SConditionCompareType.LessOrEqual:
                    return comparable != null && comparable.CompareTo(numB) <= 0;
                default:
                    throw new Exception("can't resolved type");
            }
        }
    }
}
