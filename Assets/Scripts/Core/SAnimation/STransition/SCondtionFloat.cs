﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;


namespace StgMaker.Core
{
    public class SConditionFloat : SCondition
    {
        private string _name;
        private float _value;
        private SConditionCompareType _type;

        public SConditionFloat(string name, float value, SConditionCompareType cmpType)
        {
            _name = name;
            _value = value;
            _type = cmpType;
        }

        public override bool Calculate(VarPool vars)
        {
            float value = vars.GetVarValue<float>(_name);
            return SConditionCompare.Compare(value, _value, _type);
        }
    }
}
