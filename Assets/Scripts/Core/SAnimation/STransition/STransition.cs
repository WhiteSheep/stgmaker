﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace StgMaker.Core
{
    public class STransition
    {
        public SAnimationState targetState;
        public STransitionType transitionType = STransitionType.And;
        private List<SCondition> _conditions = new List<SCondition>(); 

        public STransition() { }

        public STransition(SAnimationState target)
        {
            targetState = target;
        }

        public void AddCondition(SCondition cond)
        {
            _conditions.Add(cond);
        }

        public void RemoveCondition(SCondition cond)
        {
            _conditions.Remove(cond);
        }

        public bool NeedTransition(VarPool pool)
        {
            //  no condition --- return true
            if (_conditions.Count == 0)
                return true;

            foreach (SCondition condition in _conditions)
            {
                bool value = condition.Calculate(pool);

                //  one false --- return false
                if (transitionType == STransitionType.And && !value)
                    return false;

                //  one true --- return true
                if (transitionType == STransitionType.Or && value)
                    return true;
            }

            //  all condition is true
            if (transitionType == STransitionType.And)
                return true;

            //  all condition is false
            if (transitionType == STransitionType.Or)
                return false;

            return false;
        }
    }
}
