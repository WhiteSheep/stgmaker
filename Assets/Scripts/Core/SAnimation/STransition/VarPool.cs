﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;

namespace StgMaker.Core
{
    public class Variable
    {
        public string name;
        public object value;
        public bool isTrigger;

        public Variable() {}

        public Variable(string varName, object varValue, bool varIsTrigger = false)
        {
            name = varName;
            value = varValue;
            isTrigger = varIsTrigger;
        }
    }

    public class VarPool
    {
        public Dictionary<string, Variable> pool = new Dictionary<string, Variable>();

        public void Add(Variable var)
        {
            pool.Add(var.name, var);
        }

        public void Remove(string varname)
        {
            pool.Remove(varname);
        }

        public T GetVarValue<T>(string varname)
        {
            object o = GetVarValue(varname);
            if (o != null)
                return (T) o;

            return default(T);
        }

        public object GetVarValue(string varname)
        {
            Variable var = null;
            pool.TryGetValue(varname, out var);

            return (var != null) ? var.value : null;
        }

        public void SetVarValue(string varname, object value)
        {
            Variable var = null;
            pool.TryGetValue(varname, out var);

            if (var != null)
                var.value = value;
        }

        public Variable GetVariable(string varname)
        {
            Variable var = null;
            pool.TryGetValue(varname, out var);

            return var;
        }

        public void Refresh()
        {
            //  Refresh all variable
            foreach (var kv in pool)
            {
                Variable var = kv.Value;
                if (var.isTrigger)
                    var.value = false;
            }
        }
    }
}
