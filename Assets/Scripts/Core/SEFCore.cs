﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StgMaker.InputModule;
using UnityEngine;


namespace StgMaker.Core
{
    public class SEFCore : MonoBehaviour
    {
        private static SEFCore _inst = null;
        private SEFTriggerMgr _triggerMgr;
        private SEFActionHandlerMgr _actionHandlerMgr;
        private SEFSpriteMgr _spriteMgr;
        private SEFAnimationMgr _animationMgr;

        public static SEFCore Instance()
        {
            if (_inst == null)
                _inst = new SEFCore();
            return _inst;
        }

        protected SEFCore()
        {
            Setup();
        }

        public void Awake()
        {
            if (_inst == null)
            {
                _inst = this;
                DontDestroyOnLoad(this);
            }
            else
                Destroy(gameObject);
        }

        public void Setup()
        {
            _triggerMgr = Locator.GetTriggerMgr();
            _actionHandlerMgr = Locator.GetActionHandlerMgr();
            _spriteMgr = Locator.GetSpriteMgr();
            _animationMgr = Locator.GetAnimationMgr();
        }

        public void Update()
        {
            //  Listen input and create signal
            _ListenInput();

            //  Update manager in one frame with order
            _triggerMgr.Update();
            _actionHandlerMgr.Update();
            _spriteMgr.Update();
            _animationMgr.Update();

            //  Some Logic update code ...

        }

        public void FixedUpdate()
        {
            _triggerMgr.FixedUpdate();
            _spriteMgr.FixedUpdate();
        }

        public void LateUpdate()
        {
            _triggerMgr.LateUpdate();
            _actionHandlerMgr.LateUpdate();
            _spriteMgr.LateUpdate();
            _animationMgr.LateUpdate();
        }

        public void OnDestroy()
        {
            _actionHandlerMgr.OnDestroy();
        }

        private void _ListenInput()
        {
            if (InputManager.AnyKeyDown())
            {
                EventSignal keyDownSignal = new EventSignal(EventType.EventOnKeyDown);
                _triggerMgr.AddSignal(keyDownSignal);
            }

            if (InputManager.AnyKey())
            {
                EventSignal keyPressedSignal = new EventSignal(EventType.EventOnKeyPressed);
                _triggerMgr.AddSignal(keyPressedSignal);
            }
        }
    }
}
