﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

namespace StgMaker.Core
{
    public class SpriteObject : IGameUpdator
    {
        //  Base properties 
        public SpriteObjectLifeState state = SpriteObjectLifeState.Initialization;
        public SpriteObjectCampType camp = SpriteObjectCampType.Neutral;
        public SpriteObjectType type;
        public int uid;
        public int hp;
        public int atk;
        public int maxLifeTime;
        public Vector2 pos = new Vector2();

        //  Binding GameObject properties
        protected GameObject _go;
        protected SpriteRenderer _spriteRenderer;
        protected Rigidbody2D _rigidbody2D;

        //  Animation Controller
        protected SAnimationController _animationController;

        //  Calculate
        protected Vector2 _lastPos = new Vector2();
        protected Vector2 _accel = Vector2.zero;
        protected int _elapsedFrame = 0;

        public int ElapsedFrame
        {
            get { return _elapsedFrame;}
        }


        public SpriteObject()
        {
            uid = LuaExp.CreateUID();
        }

        public virtual void Init()
        {
            _go = new GameObject("SpriteObject_" + uid);

            //  CreateSpriteRender
            _spriteRenderer = _go.AddComponent<SpriteRenderer>();

            //  Create Physics System
            _rigidbody2D = _go.AddComponent<Rigidbody2D>();
            _rigidbody2D.gravityScale = 0;
            _rigidbody2D.isKinematic = false;

            //  Create SpriteBinder
            SpriteObjectBinder binder = _go.AddComponent<SpriteObjectBinder>();
            binder.origin = this;

            //  Bind to AnimationMgr
            Locator.GetAnimationMgr().RegistController(_animationController, this);
        }

        public virtual void Update()
        {
            //  In this frame, this sprite will do nothing but create a signal
            //  This signal will be used in next frame - to do something
            if (state == SpriteObjectLifeState.Initialization)
            {
                EventSignal signal = new EventSignal(EventType.EventSpriteInitialization, this);
                Locator.GetTriggerMgr().AddSignal(signal);
            }

            //  When hp <= 0, we need change state
            if (hp <= 0)
            {
                if (state == SpriteObjectLifeState.Running)
                {
                    EventSignal signal = new EventSignal(EventType.EventOnSpriteDestroying, this);
                    Locator.GetTriggerMgr().AddSignal(signal);

                    state = SpriteObjectLifeState.OnDestroying;
                }
                else if (state == SpriteObjectLifeState.OnDestroying)
                {
                    state = SpriteObjectLifeState.Destroyed;
                }
            }

            //  Calculate Sprite properties
            _accel = pos - _lastPos;
            _animationController.SetVarValue("Acceleration", _accel.x);
        }

        public virtual void FixedUpdate()
        {
        }

        public virtual void LateUpdate()
        {
            //  Zero-frame passed
            if (state == SpriteObjectLifeState.Initialization)
                state = SpriteObjectLifeState.Running;

            //  Update propeties
            _lastPos = pos;

            //  Export properties to game-scene
            _go.transform.position = new Vector3(pos.x, pos.y);

            _elapsedFrame += 1;
        }

        public virtual void OnDestroy()
        {
            //  Destroy gameobject
            _go.SetActive(false);
            SEFUnityHelper.DestroyGameObject(_go);
        }

        public virtual bool IfDestroyed()
        {
            return state == SpriteObjectLifeState.Destroyed;
        }

        public virtual void SetSAnimationController(SAnimationController prmAnimCtrl)
        {
            _animationController = prmAnimCtrl;
        }

        public virtual SpriteRenderer GetSpriteRenderer()
        {
            return _spriteRenderer;
        }

        public virtual GameObject GetBindingGameObject()
        {
            return _go;
        }

        public virtual SpriteObject SetLocalRotation(Vector3 prmRotVec3)
        {
            if (_go != null)
                _go.transform.localRotation = Quaternion.Euler(prmRotVec3);

            return this;
        }

        public SpriteObject  SetLocalScale(Vector3 prmScaVec3)
        {
            if (_go != null)
                _go.transform.localScale = prmScaVec3;

            return this;
        }

        public SpriteObject  Rotate(Vector3 prmRotAxis, float prmAngle)
        {
            if (_go != null)
                _go.transform.Rotate(prmRotAxis, prmAngle);

            return this;
        }
    }
}