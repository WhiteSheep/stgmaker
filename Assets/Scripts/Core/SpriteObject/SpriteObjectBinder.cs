﻿using UnityEngine;
using System.Collections;

namespace StgMaker.Core
{
    public class SpriteObjectBinder : MonoBehaviour
    {
        public SpriteObject origin = null;

        private void OnTriggerEnter2D(Collider2D other)
        {
            SpriteObject obj = other.gameObject.GetComponent<SpriteObjectBinder>().origin;

            //  Check identification friend or foe (IFF)
            if (SpriteObjectHelper.CheckIFF(origin, obj))
            {
                //  Create under attack signal
                EventSignal collideSignal = new EventSignal(
                EventType.EventOnSpriteUnderAttack, origin);

                //  reduce origin hpValue and create singal
                origin.hp -= obj.atk;
                EventSignal hpChangedSignal = new EventSignal(
                    EventType.EventOnSpriteHpChanged, origin);

                Locator.GetTriggerMgr().AddSignal(collideSignal, hpChangedSignal);    
            }

            
        }
    }
}