﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class SpriteObjectHelper
    {
        //  return true if is foe, else friend
        public static bool CheckIFF(SpriteObject objA, SpriteObject objB)
        {
            //  Neutral return false
            if (objA.camp == SpriteObjectCampType.Neutral
                || objB.camp == SpriteObjectCampType.Neutral)
                return false;

            // xor calculation
            return !(objA.camp == SpriteObjectCampType.Confederate ^
                    objB.camp == SpriteObjectCampType.Opponent);
        }
    }
}
