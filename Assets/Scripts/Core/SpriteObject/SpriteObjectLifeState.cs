﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public enum SpriteObjectLifeState
    {
        Initialization, //  when new sprite added to system, it will send a signal
        Running,        //  sprite running
        OnDestroying,   //  when logic check find something ready to destroyed, it will send a signal
        Destroyed       //  sprite destroyed --> add score?
    }
}
