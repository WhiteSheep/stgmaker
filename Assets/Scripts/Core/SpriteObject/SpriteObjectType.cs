﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public enum SpriteObjectType
    {
        EnemySprite,
        PlayerSprite,
        PlayerBullet,
        EnemyBullet,
        SpriteUnit,
        SpriteEffect,
        Unknown
    }
}
