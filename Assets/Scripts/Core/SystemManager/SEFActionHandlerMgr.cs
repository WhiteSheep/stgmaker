﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class SEFActionHandlerMgr : IGameUpdator
    {
        public virtual void AddActionHandler(ActionHandler actionHdl) { }

        public virtual void Update() { }

        public virtual void LateUpdate() { }

        public virtual void FixedUpdate() { }

        public virtual void OnDestroy() { }
    }
}
