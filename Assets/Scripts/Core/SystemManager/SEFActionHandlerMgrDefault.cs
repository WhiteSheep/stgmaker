﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking.Match;
using Debug = UnityEngine.Debug;

namespace StgMaker.Core
{
    public class SEFActionHandlerMgrDefault : SEFActionHandlerMgr
    {
        protected List<ActionHandler> _actionHdlList;
        
        public SEFActionHandlerMgrDefault()
        {
            _actionHdlList = new List<ActionHandler>();
        }

        public override void AddActionHandler(ActionHandler actionHdl)
        {
            _actionHdlList.Add(actionHdl);
        }

        public override void Update()
        {
            foreach (ActionHandler actHdl in _actionHdlList)
            {
                //  Only valid and isReady actHdl can run
                if (actHdl.IsValid())
                {
                    if (actHdl.IsReady())
                        actHdl.Run();
                }
            }
        }

        public override void LateUpdate()
        {
            List<ActionHandler> removedActionHandlers = new List<ActionHandler>();

            //  Update every valid actHdl
            foreach (ActionHandler actHdl in _actionHdlList)
            {
                if (actHdl.IsValid())
                    actHdl.LateUpdate();
                else
                    removedActionHandlers.Add(actHdl);
            }

            //  Removed finished ActionHandler
            foreach (ActionHandler actHdl in removedActionHandlers)
            {
                actHdl.OnDestroy();
                _actionHdlList.Remove(actHdl);
            }
            removedActionHandlers.Clear();
        }

        public override void OnDestroy()
        {
            foreach (ActionHandler actHdl in _actionHdlList)
            {
                actHdl.OnDestroy();
            }
        }
    }
}
