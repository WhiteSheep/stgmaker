﻿using UnityEngine;
using System;
using System.Collections;
using Object = UnityEngine.Object;

namespace StgMaker.Core
{
    public class SEFResourceMgr
    {
        public virtual Object Load(string id)
        {
            Debug.LogError("Please override this method!");
            return null;
        }

        public virtual T Load<T>(string id) where T : Object
        {
            Debug.LogError("Please override this method!");
            return default(T);
        }

        public virtual void RegistResource(string id, string resPath, Type type) { }
        public virtual void RegistResource<T>(string id, string resPath) { }
    }
}

