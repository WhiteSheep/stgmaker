﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;

namespace StgMaker.Core
{
    public class SEFResourceMgrDefault : SEFResourceMgr
    {
        private Dictionary<string, Object> _resource;

        public SEFResourceMgrDefault()
        {
            _resource = new Dictionary<string, Object>();
        }

        public override void RegistResource(string id, string resPath, Type type)
        {
            Object res = null;
            if (type == typeof (Texture2D))
            {
                DateTime time1 = DateTime.Now;
                res = _LoadPic(resPath);
                DateTime time2 = DateTime.Now;

                Debug.Log(string.Format("{0} : {1}", resPath, time2 - time1));
            }

            if (res)
                _resource.Add(id, res);
        }

        public override void RegistResource<T>(string id, string resPath)
        {
            RegistResource(id, resPath, typeof(T));
        }

        public override Object Load(string id)
        {
            Object obj = null;
            _resource.TryGetValue(id, out obj);

            return obj;
        }

        public override T Load<T>(string id)
        {
            return Load(id) as T;
        }

        private Texture2D _LoadPic(string filepath)
        {
            Texture2D tex2d = null;

            if (FileSysUtil.Exists(filepath))
            {
                byte[] fileBytes = FileSysUtil.LoadFileToBytes(filepath);

                tex2d = new Texture2D(2, 2, TextureFormat.DXT5, false);
                tex2d.LoadImage(fileBytes);
            }

            return tex2d;
        }
    }
}
