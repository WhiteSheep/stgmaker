﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class SEFSpriteMgr : IGameUpdator
    {
        public virtual void Update() { }
        public virtual void FixedUpdate() { }
        public virtual void LateUpdate() { }
        public virtual void Add(SpriteObject value) { }
        public virtual void OnDestroy() { }
        public virtual SpriteObject GetSpriteByUID(int uid)
        {
            return null;
        }
    }
}
