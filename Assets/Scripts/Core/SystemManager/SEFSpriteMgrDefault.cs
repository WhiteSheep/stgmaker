﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using StgMaker.Core;

namespace Assets.Scripts.Core
{
    public class SEFSpriteMgrDefault : SEFSpriteMgr
    {
        private Dictionary<int, SpriteObject> _sprites; 

        public SEFSpriteMgrDefault()
        {
            _sprites = new Dictionary<int, SpriteObject>();
        }

        public override void Update()
        {
            foreach (var kv in _sprites)
            {
                SpriteObject sprite = kv.Value;
                sprite.Update();
            }
        }

        public override void LateUpdate()
        {
            List<int> spriteUIDs = new List<int>();

            //  check need destroyed SpriteObject, and do LateUpdate
            foreach (var kv in _sprites)
            {
                SpriteObject sprite = kv.Value;

                if (sprite.IfDestroyed())
                {
                    sprite.OnDestroy();
                    spriteUIDs.Add(sprite.uid);
                }
                else
                    sprite.LateUpdate();
            }

            //  remove
            foreach (int single in spriteUIDs)
                _sprites.Remove(single);
        }

        public override void Add(SpriteObject value)
        {
            _sprites.Add(value.uid, value);
        }

        public override SpriteObject GetSpriteByUID(int uid)
        {
            SpriteObject retSprite;
            _sprites.TryGetValue(uid, out retSprite);
            return retSprite;
        }
    }
}
