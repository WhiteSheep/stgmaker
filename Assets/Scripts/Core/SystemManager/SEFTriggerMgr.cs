﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StgMaker.Core
{
    public class SEFTriggerMgr : IGameUpdator
    {
        public virtual void AddTrigger(Trigger t) { }
        public virtual void AddSignal(EventSignal e) { }
        public virtual void AddSignal(params EventSignal[] e) { }
        public virtual void Update() { }
        public virtual void LateUpdate() { }
        public virtual void FixedUpdate() { }
        public virtual void OnDestroy() { }
        
    }
}
