﻿using System.Collections.Generic;
using UnityEngine;

namespace StgMaker.Core
{
    public class SEFTriggerMgrDefault : SEFTriggerMgr
    {
        protected List<Trigger> _triggers;
        protected List<EventSignal> _signals;

        public SEFTriggerMgrDefault()
        {
            _triggers = new List<Trigger>();
            _signals = new List<EventSignal>();
        }

        public override void AddTrigger(Trigger t)
        {
            _triggers.Add(t);
        }

        public override void AddSignal(EventSignal e)
        {
            _signals.Add(e);
        }

        public override void AddSignal(params EventSignal[] e)
        {
            foreach (var signal in e)
            {
                _signals.Add(signal);
            }
        }

        public override void Update()
        {
            //  tell each trigger all events
            foreach (Trigger t in _triggers)
            {
                //  Refresh valid prop
                t.Update();

                //  Only valid trigger can be noticed
                if (t.IsValid())
                {
                    if (_signals.Count == 0)
                        t.Notice(EventSignal.NoEvent);
                    else
                        foreach (EventSignal e in _signals)
                            t.Notice(e);    
                }
            }
            _signals.Clear();
        }

        public override void LateUpdate()
        {
            //  refresh trigger if valid
            List<Trigger> removedTriggers = new List<Trigger>();
            foreach (Trigger trigger in _triggers)
            {
                if (trigger.IsValid())
                    trigger.LateUpdate();
                else
                    removedTriggers.Add(trigger);
            }

            //  remove trigger which can not be triggered forever
            foreach (var trigger in removedTriggers)
            {
                _triggers.Remove(trigger);
            }
        }

        public override void OnDestroy()
        {
            foreach (Trigger trigger in _triggers)
            {
                trigger.OnDestroy();
            }
        }
    }
}