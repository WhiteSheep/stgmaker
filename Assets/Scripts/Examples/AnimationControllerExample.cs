﻿using UnityEngine;
using System.Collections;

using StgMaker.Core;

public class AnimationControllerExample : MonoBehaviour
{
    private SAnimationController controller;
    private GameObject spriteA;
	// Use this for initialization
	void Start () {
        spriteA = GameObject.Find("SpriteA");
        controller = new SAnimationController("TestController");

        //  Regist variable
        Variable finished = new Variable("finished", 0);
        Variable tempfloat = new Variable("toleft", false); 
        controller.AddVar(new []{finished, tempfloat});

        //  Load SpriteSheet
        Texture2D tex2d = Resources.Load<Texture2D>("pl00");
        SSpriteSheet spriteSheet = new SSpriteSheet();
        spriteSheet.Slice(tex2d, 32, 48, 24);

        //  Create all state
        SAnimationState frontState = new SAnimationState("front");
        SAnimationState toleftState = new SAnimationState("toleft");
        SAnimationState leftState = new SAnimationState("left");

        // -----------  Set state info  ------------

        //  front
        frontState.motion = new SAnimationClip(spriteSheet.SubSprites(0, 8));

        STransition transition = new STransition(controller.ExitState);
        transition.AddCondition(new SConditionInt("finished", 600, SConditionCompareType.GreaterOrEqual));
        frontState.AddTransition(transition);
        
        STransition trans_toLeft = new STransition(toleftState);
        trans_toLeft.AddCondition(new SConditionBool("toleft", true));
	    frontState.AddTransition(trans_toLeft);

        //  toleft
        toleftState.motion = new SAnimationClip(spriteSheet.SubSprites(8, 4));
        toleftState.AddTransition(new STransition(leftState));

        //  left
        leftState.motion = new SAnimationClip(spriteSheet.SubSprites(12, 4));
        
        STransition trans_front = new STransition(frontState);
        trans_front.AddCondition(new SConditionBool("toleft", false));
        leftState.AddTransition(trans_front);

        //  finally
        controller.AddState(frontState);
        controller.SetEntryState(frontState);
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (!controller.IsFinish())
	    {
            if (Time.frameCount % 240 == 0)
            {
                if (Time.frameCount % 480 == 0)
                    controller.SetVarValue("toleft", false);
                else
                    controller.SetVarValue("toleft", true);
            }

            if (Time.frameCount == 600)
                controller.SetVarValue("finished", 600);

            spriteA.GetComponent<SpriteRenderer>().sprite = controller.Update();    
	    }
	    
	}

    void LateUpdate()
    {
        controller.LateUpdate();
    }
}
