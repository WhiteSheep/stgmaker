﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using LuaInterface;
using StgMaker.Collision;

namespace StgMaker.Core
{
    public static class LuaExp
    {
        private static LuaScriptMgr _lua;
        private static int _uIdInc = 0;

        static LuaExp()
        {
            _lua = new LuaScriptMgr();
            _lua.Start();
        }

        public static LuaTable NewLuaTable()
        {
            return _lua.lua.NewTable();
        }

        public static object[] DoString(string luaStr)
        {
            return _lua.DoString(luaStr);
        }

        public static int CreateUID()
        {
            return ++_uIdInc;
        }

        public static void ShowLog(string val)
        {
            Debug.Log(val);
        }

        public static void RegistSpriteObjectWrapper(LuaTable wrapper)
        {
            //  CreateSpriteObject
            SpriteObject spriteObj = (SpriteObject)wrapper["spriteObject"];

            //  Create AnimationController
            LuaTable animationGroup = (LuaTable) wrapper["animationGroup"];
            SAnimationController animationController = BuildInAnimationController.Create(animationGroup);
            spriteObj.SetSAnimationController(animationController);

            //  Init
            spriteObj.Init();
            Locator.GetSpriteMgr().Add(spriteObj);

            //  Creater Colliders
            LuaTable colliders = (LuaTable) wrapper["colliders"];
            IDictionaryEnumerator cItor = colliders.GetEnumerator();
            while (cItor.MoveNext())
            {
                ColliderConf colliderConf = (ColliderConf) cItor.Value;
                ColliderHelper.BindColliderToGameObject(spriteObj.GetBindingGameObject(), colliderConf);
            }

            //  Create Triggers
            LuaTable triggers = (LuaTable) wrapper["triggers"];
            IDictionaryEnumerator tItor = triggers.GetEnumerator();
            while (tItor.MoveNext())
            {
                LuaTable item = (LuaTable) tItor.Value;
                RegistTrigger(item);
            }
        }

        public static void RegistSprite(LuaTable spriteTab)
        {
            SpriteObject sprite = new SpriteObject();

            //  Regist sprite
            sprite.uid = Convert.ToInt32(spriteTab["uid"]);
            sprite.hp = Convert.ToInt32(spriteTab["hp"]);
            sprite.pos = new Vector2(Convert.ToSingle(spriteTab["posX"]),
                Convert.ToSingle(spriteTab["posY"]));
            sprite.type = (SpriteObjectType) spriteTab["type"];
            sprite.atk = Convert.ToInt32(spriteTab["atkValue"]);

            //  Set Sprite's model
            LuaTable animationGroup = (LuaTable) spriteTab["animationGroup"];
            SAnimationController animationController = BuildInAnimationController.Create(animationGroup);
            sprite.SetSAnimationController(animationController);

            //  Initialization
            sprite.Init();
            Locator.GetSpriteMgr().Add(sprite);

            //  Create Collider
            LuaTable colliders = (LuaTable) spriteTab["colliders"];
            IDictionaryEnumerator cItor = colliders.GetEnumerator();
            while (cItor.MoveNext())
            {
                ColliderConf colliderConf = (ColliderConf) cItor.Value;
                ColliderHelper.BindColliderToGameObject(sprite.GetBindingGameObject(), colliderConf); 
            }

            //  Regist all trigger's info
            LuaTable triggers = (LuaTable) spriteTab["triggers"];
            IDictionaryEnumerator tItor = triggers.GetEnumerator();
            while (tItor.MoveNext())
            {
                LuaTable item = (LuaTable) tItor.Value;
                RegistTrigger(item);
            }
        }

        public static void RegistTrigger(LuaTable triggerTab)
        {
            Trigger trigger = new Trigger();

            //  Get trigger uid and bid
            trigger.uid = Convert.ToInt32(triggerTab["uid"]);
            trigger.bid = Convert.ToInt32(triggerTab["bid"]);
            trigger.binder = Locator.GetSpriteMgr().GetSpriteByUID(trigger.bid);
            if (trigger.bid != -1)
                trigger.ifBind = true;

            //  Load all events
            LuaTable events = (LuaTable)triggerTab["event"];
            IDictionaryEnumerator itor = events.GetEnumerator();
            while (itor.MoveNext())
            {
                //  Load each event
                LuaTable item = (LuaTable)itor.Value;
                EventType eventType = (EventType)Convert.ToInt32(item["eventType"]);

                //  Load event's params
                LuaTable eventParams = (LuaTable)item["params"];
                IDictionaryEnumerator prmItor = eventParams.GetEnumerator();
                List<object> paramList = new List<object>();

                while (prmItor.MoveNext())
                {
                    object prm = (object)prmItor.Value;
                    paramList.Add(prm);
                }

                //  Create event
                Event e = EventCreator.Create(eventType, paramList);
                if (e != null)
                    trigger.AddEvent(e);
            }

            //  Load condition
            LuaFunction condFunc = (LuaFunction)triggerTab["condition"];
            Condition cond = new Condition(condFunc);
            trigger.AddCondition(cond);

            //  Load action
            LuaTable actions = (LuaTable)triggerTab["action"];
            IDictionaryEnumerator actItor = actions.GetEnumerator();

            while (actItor.MoveNext())
            {
                LuaTable actTable = (LuaTable)actItor.Value;
                SimpleAction action = new SimpleAction(actTable, "Run");
                trigger.AddAction(action);
            }

            Locator.GetTriggerMgr().AddTrigger(trigger);
        }
    }

}
