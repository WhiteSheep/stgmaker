﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LuaInterface;
using StgMaker.Core;

using Object = UnityEngine.Object;
using Sprite = UnityEngine.Sprite;

public class TestExp : MonoBehaviour
{
    private Animator animator;
    private bool button;
    // Use this for initialization
    private void Start()
    {
        string gameConfigPath = Application.dataPath + "/Resources/DemoStage/GameConfig.lua";
        if (FileSysUtil.Exists(gameConfigPath))
        {
            string content = FileSysUtil.LoadFileToString(gameConfigPath);
            LuaExp.DoString(content);
        }

        string path = Application.dataPath + "/Resources/DemoStage/stage_01.lua";
        if (FileSysUtil.Exists(path))
        {
            string content = FileSysUtil.LoadFileToString(path);
            LuaExp.DoString(content);
        }
    }

    // Update is called once per frame
    private void Update()
    {
    }
}