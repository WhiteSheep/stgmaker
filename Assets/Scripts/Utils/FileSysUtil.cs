﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

public class FileSysUtil
{
    public static bool Exists(string filename)
    {
        return File.Exists(filename);
    }

    public static string FindFile(string basepath, string filename)
    {   
        string[] files = Directory.GetFiles(basepath, filename, 
            SearchOption.AllDirectories);

        if (files.Length == 0)
            return null;

        return files[0];
    }

    public static string LoadFileToString(string filename)
    {
        return File.ReadAllText(filename);
    }

    public static byte[] LoadFileToBytes(string filename)
    {
        return File.ReadAllBytes(filename);
    }
}