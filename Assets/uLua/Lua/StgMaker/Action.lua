require "StgMaker\\CoHelper"

local setmetatable = setmetatable
local rawget = rawget

Action = {
--[[ actionFunc,
	param = {},
	
	class = "Action"
--]]
}

setmetatable(Action, Action)
Action.__index = function(t, k)
	return rawget(Action, k)
end

function EmptyFunction(e) print("empty func") end

function Action.New(func, ...)
	local o = {
		actionFunc = func or EmptyFunction,
		params = {...} or {}
	}
	
	setmetatable(o, Action)
	return o
end

function Action:AddParams(...)
	for i,v in ipairs{...} do
		self.params[#self.params + 1] = v
	end
end

function Action:ClearAllParams()
	for k in pairs(self.params) do
		self.params[k] = nil
	end
end

function Action:Run(eArgs)
	local coHelper = CoHelper.New(eArgs, self.actionFunc, unpack(self.params))
	return coHelper
end