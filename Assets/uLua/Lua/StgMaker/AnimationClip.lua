require "StgMaker\\CLBase"

AnimationClipType = {
	Grid = 0,
	CustomRect = 1
}

AnimationClip = {
	--	type
	clipType,

	--	grid type
	gridWidth,
	gridHeight,
	textureId,
	startPos,
	length,
	frameCount,

	--	custom rect type
	customRects
}

function AnimationClip:new(o)
	o = o or {}
	setmetatable(o, {__index = self})
	o:Init()
	return o
end

function AnimationClip:Init()
	self.customRects = {}
end

function AnimationClip:AddCustomRect(...)
	self.clipType = AnimationClipType.CustomRect
	
	for i,v in ipairs{...} do
		self.customRects[#self.customRects + 1] = v
	end
end

function AnimationClip.CreateWithGrid(textureId, gridWidth, gridHeight, startPos, length, frameCount)
	local animClip = AnimationClip:new()
	animClip.clipType = AnimationClipType.Grid
	animClip.textureId = textureId
	animClip.gridWidth = gridWidth
	animClip.gridHeight = gridHeight
	animClip.startPos = startPos
	animClip.length = length
	animClip.frameCount = frameCount or 0
	
	return animClip
end

function AnimationClip.CreateWithCustomedRect(textureId, frameCount, ...)
	local animClip = AnimationClip:new()
	animClip.clipType = AnimationClipType.CustomRect
	animClip.frameCount = frameCount or 0
	animClip.textureId = textureId
	animClip:AddCustomRect(...)
	
	return animClip
end
