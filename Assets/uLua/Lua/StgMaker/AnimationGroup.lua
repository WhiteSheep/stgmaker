require "StgMaker\\CLBase"

--	AnimationControllerType
AnimationControllerType = {
	SimpleEnemy = 0
}

--	----------	Animation Group	----------	--
AnimationGroup = {
	animationControllerType,
	animationClips
}

function AnimationGroup:new(o)
	o = o or {}
	setmetatable(o, {__index = self})
	o:Init()
	return o
end

function AnimationGroup:Init()
	self.animationClips = {}
end

function AnimationGroup:SetControllerType(ctrlType)
	self.animationControllerType = ctrlType
end

function AnimationGroup:AddAnimationClip(animStateType, clip)
	local oneState = {}
	oneState.animStateType = animStateType
	oneState.clip = clip
	
	self.animationClips[#self.animationClips + 1] = oneState
end

function AnimationGroup.Create()
	local animGroup = AnimationGroup:new()
	return animGroup
end