---	Initialize all system functions
luanet.load_assembly("System")
luanet.load_assembly("UnityEngine")

--	Unity Class/Enum
KeyCode = UnityEngine.KeyCode

--	Extern Class/Enum
LuaExp = System.LuaExp
Input = System.SEFInputManager
EventType = System.SEFEventType
BuildInVirtualButton = System.SEFBuildInVirtualButton
BuildInAnimationStateType = System.SEFBuildInAnimationStateType
BuildInAnimationControllerType = System.SEFBuildInAnimationControllerType
RuntimeHelper = System.SEFRuntimeHelper
SpriteObject = System.SEFSpriteObject
SpriteObjectType = System.SEFSpriteObjectType
SpriteObjectCampType = System.SEFSpriteObjectCampType
ColliderType = System.SEFColliderType
Collider = System.SEFColliderConf

--	Lua
yield = coroutine.yield

--	Env config
Env = {
	ProjectPath = 0
}

--	System function
sys = {}
function sys.CreateUID()
	return LuaExp.CreateUID()
end

function sys.Log(val)
	return LuaExp.ShowLog(val)
end
