require "StgMaker\\CLBase"
require "StgMaker\\Trigger"

CL = {}

--	-------------------	Load Resource To System ------------------- --
function CL.RegistSystemEnv(envItem, value)
	LuaExp.RegistSystemEnv(envItem, value)
end

function CL.LoadTexture2D(id, filepath)
	LuaExp.LoadTexture2D(id, filepath)
end

--	-------------------	Trigger Interfaces ------------------- --
--	Regist a trigger data to engine
function CL.RegistTriggerToSEF(val)
	LuaExp.RegistTrigger(val)
end

--	Used to create trigger for stage lua
function CL.EasyStageTrigger(frame,actionFunc,...)
	local t = Trigger.New()
	t:AddEvents(Event.New(EventType.OnFrameArrive, frame))
	t:AddActions(Action.New(actionFunc, ...))
	
	CL.RegistTriggerToSEF(t)
end

--	-------------------	Sprites Interfaces ------------------- --
--	Regist a sprite data to engine
function CL.RegistSpriteToSEF(val)
	LuaExp.RegistSprite(val)
end

function CL.PlaySpriteObjectWrapper(val)
	LuaExp.RegistSpriteObjectWrapper(val)
end

--	-------------------	System APIs ------------------- --
--	Run a lua file, directory is redirect to /Asset/Resource/
function CL.DoFile(filename)
	return LuaExp.DoFile(filename)
end

function CL.RunScript(filename, ...)
	return LuaExp.RunScript(filename, ...)
end

--	-------------------	Game APIs ------------------- --
--	Get frameCount
function CL.GetFrameCount()
	return LuaExp.GetFrameCount()
end