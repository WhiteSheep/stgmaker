require "StgMaker\\CLCore"
require "StgMaker\\Event"
require "StgMaker\\Action"
require "StgMaker\\Trigger"
require "StgMaker\\SpriteObjectWrapper"
require "StgMaker\\AnimationGroup"
require "StgMaker\\AnimationClip"
require "StgMaker\\Rect"