require "StgMaker\\CLBase"

local create = coroutine.create
local resume = coroutine.resume
local setmetatable = setmetatable
local rawget = rawget


CoHelper = { 
--[[ co,
	params = {},
	eventArgs = {}
	
	class == "CoHelper" 
--]]
}

setmetatable(CoHelper, CoHelper)

CoHelper.__index = function(t, k)
	return rawget(CoHelper, k)
end

function CoHelper.New(eArgs, func, ...)
	local o = 
	{
		eventArgs = eArgs or {},
		co = create(func),
		params = {...} or {}
	}
	
	setmetatable(o, CoHelper)
	return o
end

function CoHelper:Run()
	--	refresh eventArgs state
	if (self.eventArgs.binderId ~= -1) then
		self.eventArgs.binder = RuntimeHelper.GetSpriteObjectByID(self.eventArgs.binderId)
	end
	
	if (coroutine.status(self.co) ~= "dead") then
		return resume(self.co, self.eventArgs, unpack(self.params))
	end
end