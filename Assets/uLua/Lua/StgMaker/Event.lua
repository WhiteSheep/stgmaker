Event = 
{
--[[	eventType,
		params = {},
		
		class = "Event"
--]]
}

setmetatable(Event, Event)
Event.__index = function(t, k)
	return rawget(Event, k)
end

function Event.New(eType, ...)
	local o = 
	{
		eventType = eType or nil,
		params = {...} or {}
	}
	
	setmetatable(o, Event)
	return o
end

function Event:AddParams(...)
	for i,v in ipairs{...} do
		self.params[#self.params + 1] = v
	end
end

function Event:ClearAllParams()
	for k in pairs(self.params) do
		self.params[k] = nil
	end
end