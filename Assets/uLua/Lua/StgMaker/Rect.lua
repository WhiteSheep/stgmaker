SliceDirection = {
	Horizonal = 0,
	Vertical = 1
}

Rect = {
	x,y,w,h,
	direction,
	length
}

function Rect:new(o)
	o = o or {}
	setmetatable(o, {__index = self})
	return o
end

function Rect.Create(x,y,w,h,sliceDirection,length)
	local rect = Rect:new()
	rect.x = x or 0
	rect.y = y or 0
	rect.w = w or 0
	rect.h = h or 0
	rect.direction = sliceDirection or SliceDirection.Horizonal
	rect.length = length or 1
	return rect
end