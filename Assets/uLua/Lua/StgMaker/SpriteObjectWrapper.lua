require "StgMaker\\CLBase"

SpriteObjectWrapper = 
{
--[[
	spriteObject,
	animationGroup,
	triggers,
	colliders
--]]
}

setmetatable(SpriteObjectWrapper, SpriteObjectWrapper)

local fields = {}

SpriteObjectWrapper.__index = function(t, k)
	local var = rawget(SpriteObjectWrapper, k)
	
	if var == nil then
		var = rawget(fields, k)
		if var ~= nil then
			return var(t)
		end
	end
	
	return var
end

function SpriteObjectWrapper.New(spriteObj)
	local v = 
	{
		spriteObject = spriteObj or nil,
		animationGroup,
		triggers = {},
		colliders = {}
	}

	setmetatable(v, SpriteObjectWrapper)
	return v
end

function SpriteObjectWrapper:RegistSpriteObject(val)
	self.spriteObject = val
end

function SpriteObjectWrapper:RegistTrigger(...)
	for i,v in ipairs{...} do
		self.triggers[#self.triggers + 1] = v
		
		v.bid = self.spriteObject.id
	end
end

function SpriteObjectWrapper:RegistAnimationGroup(val)
	self.animationGroup = val
end

function SpriteObjectWrapper:RegistColliders(...)
	for i,v in ipairs{...} do
		self.colliders[#self.colliders + 1] = v
	end
end