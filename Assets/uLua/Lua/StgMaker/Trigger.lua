require "StgMaker\\CLBase"

Trigger = {
--[[	event,
	condition,
	action,
	
	uid,
	bid,
	active
--]]
}

setmetatable(Trigger, Trigger)
Trigger.__index = function(t, k)
	return rawget(Trigger, k)
end

function Trigger.New()
	local o = 
	{
		event = {},
		condition = function() return true end,
		action = {},
		uid = sys.CreateUID(),
		bid = -1,
		active = true
	}
	
	setmetatable(o, Trigger)
	return o
end

function Trigger:AddEvents(...)
	for i,v in ipairs{...} do
		self.event[#self.event + 1] = v 
	end
end

function Trigger:ClearAllEvents()
	for k in pairs(self.event) do
		self.event[k] = nil
	end
end

function Trigger:AddCondition(condFunc)
	self.condition = condFunc
end

function Trigger:AddActions(...)
	for i,v in ipairs{...} do
		self.action[#self.action + 1] = v
	end
end

function Trigger:ClearAllActions()
	for k in pairs(self.action) do
		self.action[k] = nil
	end
end

function Trigger:SwitchTrigger(switchVal)
	self.active = switchVal
end