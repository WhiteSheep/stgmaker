﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;

public class LuaWrapHelper
{
    public static void CheckLuaFieldValid(IntPtr L, int index, string prmFieldName)
    {
        object o = LuaScriptMgr.GetVarObject(L, index);

        if (o == null)
        {
            LuaTypes type = LuaDLL.lua_type(L, index);
            if (type == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, string.Format("unknown member name {0}", prmFieldName));
            }
            else
            {
                LuaDLL.luaL_error(L, string.Format("attempt to index {0} on a nil value", prmFieldName));
            }
        }
    }
}