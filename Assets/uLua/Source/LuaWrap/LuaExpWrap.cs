﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using LuaInterface;
using StgMaker.Core;
using UnityEngine;
using UnityEngine.Networking;

public class LuaExpWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] regs = new LuaMethod[]
        {
            new LuaMethod("ShowLog", ShowLog),
            new LuaMethod("CreateUID", CreateUID),
            new LuaMethod("RegistTrigger", RegistTrigger),
            new LuaMethod("RegistSprite", RegistSprite),
            new LuaMethod("RegistSpriteObjectWrapper", RegistSpriteObjectWrapper), 
            new LuaMethod("DoFile", DoFile),
            new LuaMethod("LoadTexture2D", LoadTexture2D),
            new LuaMethod("RegistSystemEnv", RegistSystemEnv),
            new LuaMethod("GetFrameCount", GetFrameCount),
            new LuaMethod("RunScript", RunScript)
        };

        LuaField[] fields = new LuaField[]
        {
        };

        LuaScriptMgr.RegisterLib(L, "System.LuaExp", typeof (LuaExp), regs, fields, typeof (object));
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int ShowLog(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 1);
        string obj = (string) LuaScriptMgr.GetString(L, 1);
        LuaExp.ShowLog(obj);
        LuaScriptMgr.Push(L, obj);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int CreateUID(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 0);
        int uid = LuaExp.CreateUID();
        LuaScriptMgr.Push(L, uid);

        return 1;
    }


    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int RegistTrigger(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 1);
        LuaTable triggerTab = LuaScriptMgr.GetLuaTable(L, 1);

        LuaExp.RegistTrigger(triggerTab);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]

    private static int RegistSprite(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 1);
        LuaTable spriteTab = LuaScriptMgr.GetLuaTable(L, 1);

        LuaExp.RegistSprite(spriteTab);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int RegistSpriteObjectWrapper(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 1);
        LuaTable wrapperTab = LuaScriptMgr.GetLuaTable(L, 1);

        LuaExp.RegistSpriteObjectWrapper(wrapperTab);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int DoFile(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 1);
        string filename = LuaScriptMgr.GetString(L, 1);
        filename = Application.dataPath + "/Resources/" + filename;

        string luastr = FileSysUtil.LoadFileToString(filename);
        object[] retVal = LuaExp.DoString(luastr);

        //  push all values into a array
        LuaScriptMgr.PushArray(L, retVal);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int RunScript(IntPtr L)
    {
        int count = LuaDLL.lua_gettop(L);

        if (count >= 1)
        {
            //  get SpriteScript's lua function
            string filename = LuaScriptMgr.GetString(L, 1);
            filename = Application.dataPath + "/Resources/" + filename;
            string luastr = FileSysUtil.LoadFileToString(filename);
            object[] retVal = LuaExp.DoString(luastr);

            LuaFunction func = (LuaFunction) retVal[0];
            
            //  get lua function's parameter
            if (count >= 2)
            {
                List<object> prmList = new List<object>();
                for (int idx = 2; idx <= count; ++idx)
                    prmList.Add(LuaScriptMgr.GetVarObject(L, idx));

                retVal = func.Call(prmList.ToArray());
            }
            else
            {
                retVal = func.Call();
            }

            LuaScriptMgr.PushArray(L, retVal);
            return 1;
        }
        else
        {
            LuaDLL.luaL_error(L, "please input Script path at least!");
        }

        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int LoadTexture2D(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 2);
        string id = LuaScriptMgr.GetString(L, 1);
        string filename = LuaScriptMgr.GetString(L, 2);

        //  find file place
        filename = FileSysUtil.FindFile(Env.Get(EnvItem.ProjectPath), filename);
        Locator.GetResourceMgr().RegistResource<Texture2D>(id, filename);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int RegistSystemEnv(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 2);
        int envNum = Convert.ToInt32(LuaScriptMgr.GetNumber(L, 1));
        string envValue = LuaScriptMgr.GetString(L, 2);

        Env.Regist((EnvItem) envNum, envValue);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetFrameCount(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 0);
        LuaScriptMgr.Push(L, Time.frameCount);
        return 1;
    }
}