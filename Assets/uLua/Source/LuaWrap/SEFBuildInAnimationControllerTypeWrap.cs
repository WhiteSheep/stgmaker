﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.Core;

public class SEFBuildInAnimationControllerTypeWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] enums = new LuaMethod[]
        {
            new LuaMethod("SimpleSprite", GetSimpleSprite),
            new LuaMethod("SpriteEffect", GetSpriteEffect),
            new LuaMethod("IntToEnum", IntToEnum), 
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFBuildInAnimationControllerType", typeof(BuildInAnimationControllerType), enums);
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetSimpleSprite(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInAnimationControllerType.SimpleSprite);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetSpriteEffect(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInAnimationControllerType.SpriteEffect);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int IntToEnum(IntPtr L)
    {
        int o = (int) LuaDLL.lua_tonumber(L, 1);
        BuildInAnimationControllerType type = (BuildInAnimationControllerType) o;
        LuaScriptMgr.Push(L, type);
        return 1;
    }

    
}