﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.Core;

class SEFBuildInAnimationStateTypeWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] enums = new LuaMethod[]
        {
            new LuaMethod("Idle", GetIdle),
            new LuaMethod("ToLeft", GetToLeft),
            new LuaMethod("Left", GetLeft),
            new LuaMethod("ToRight", GetToRight),
            new LuaMethod("Right", GetRight),
            new LuaMethod("Effect", GetEffect), 
            new LuaMethod("IntToEnum", IntToEnum), 
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFBuildInAnimationStateType", typeof(BuildInAnimationStateType), enums);
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetIdle(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInAnimationStateType.Idle);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetToLeft(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInAnimationStateType.ToLeft);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetLeft(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInAnimationStateType.Left);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetToRight(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInAnimationStateType.ToRight);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetRight(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInAnimationStateType.Right);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetEffect(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInAnimationStateType.Effect);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int IntToEnum(IntPtr L)
    {
        int arg0 = (int) LuaDLL.lua_tonumber(L, 1);
        BuildInAnimationStateType o = (BuildInAnimationStateType) arg0;
        LuaScriptMgr.Push(L, o);
        return 1;
    }
}