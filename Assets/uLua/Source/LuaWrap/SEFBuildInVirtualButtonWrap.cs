﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.InputModule;

internal class SEFBuildInVirtualButtonWrap
{
    private static LuaMethod[] enums = new LuaMethod[]
    {
        new LuaMethod("Up", GetUp),
        new LuaMethod("Down", GetDown),
        new LuaMethod("Left", GetLeft),
        new LuaMethod("Right", GetRight),
        new LuaMethod("Attack", GetAttack),
        new LuaMethod("Slow", GetSlow),
        new LuaMethod("Bomb", GetBomb),
        new LuaMethod("Confirm", GetConfirm),
        new LuaMethod("Empty0", GetEmpty0),
        new LuaMethod("Empty1", GetEmpty1),
        new LuaMethod("Empty2", GetEmpty2),
        new LuaMethod("Empty3", GetEmpty3),
        new LuaMethod("Empty4", GetEmpty4),
        new LuaMethod("Empty5", GetEmpty5),
        new LuaMethod("Empty6", GetEmpty6),
        new LuaMethod("Empty7", GetEmpty7),
        new LuaMethod("Empty8", GetEmpty8),
        new LuaMethod("Empty9", GetEmpty9),
        new LuaMethod("IntToEnum", IntToEnum), 
    };

    public static void Register(IntPtr L)
    {
        LuaScriptMgr.RegisterLib(L, "System.SEFBuildInVirtualButton", typeof (BuildInVirtualButton), enums);
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetUp(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Up);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetDown(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Down);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetLeft(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Left);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetRight(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Right);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetAttack(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Attack);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetSlow(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Slow);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetBomb(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Bomb);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetConfirm(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Confirm);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty0(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty0);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty1(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty1);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty2(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty2);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty3(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty3);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty4(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty4);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty5(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty5);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty6(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty6);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty7(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty7);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty8(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty8);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEmpty9(IntPtr L)
    {
        LuaScriptMgr.Push(L, BuildInVirtualButton.Empty9);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int IntToEnum(IntPtr L)
    {
        int arg0 = (int) LuaDLL.lua_tonumber(L, 1);
        BuildInVirtualButton o = (BuildInVirtualButton) arg0;
        LuaScriptMgr.Push(L, o);
        return 1;
    }
}