﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using LuaInterface;
using StgMaker.Collision;
using UnityEngine;

public class SEFColliderConfWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] regs = new LuaMethod[]
        {
            new LuaMethod("New", _CreateColliderConf),
            new LuaMethod("AddPathPoints", AddPathPoints),
            new LuaMethod("AddEdgePoints", AddEdgePoints), 
        };  
  
        LuaField[] fields = new LuaField[]
        {
            new LuaField("type", get_type, set_type),
            new LuaField("offset", get_offset, set_offset),
            new LuaField("size", get_size, set_size),
            new LuaField("radius", get_radius, set_radius), 
            new LuaField("pathCount", get_pathCount, set_pathCount), 
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFColliderConf", typeof(ColliderConf), regs, fields, typeof(object));
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int _CreateColliderConf(IntPtr L)
    {
        int count = LuaDLL.lua_gettop(L);

        if (count == 0)
        {
            ColliderConf obj = new ColliderConf();
            LuaScriptMgr.PushObject(L, obj);
            return 1;
        }

        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int AddPathPoints(IntPtr L)
    {
        int count = LuaDLL.lua_gettop(L);

        if (count >= 6)
        {
            ColliderConf obj = (ColliderConf)LuaScriptMgr.GetVarObject(L, 1);
            int index = (int)LuaScriptMgr.GetNumber(L, 2);

            List<Vector2> vec2List = new List<Vector2>();
            for (int prmIdx = 3; prmIdx <= count; ++prmIdx)
                vec2List.Add(LuaScriptMgr.GetVector2(L, prmIdx));
            obj.AddPathPoints(index, vec2List.ToArray());    
        }
        else
        {
            LuaDLL.luaL_error(L, "AddPathPoints function param count is invalid");
        }
        
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int AddEdgePoints(IntPtr L)
    {
        int count = LuaDLL.lua_gettop(L);

        if (count >= 3)
        {
            ColliderConf obj = (ColliderConf) LuaScriptMgr.GetVarObject(L, 1);
            List<Vector2> pointList = new List<Vector2>();

            for (int idx = 2; idx <= count; ++idx)
                pointList.Add(LuaScriptMgr.GetVector2(L, idx));

            obj.AddEdgePoints(pointList.ToArray());
        }
        else
        {
            LuaDLL.luaL_error(L, "AddEdgePoints function param count is invalid");
        }

        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_type(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf) o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name type(ColliderType)");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index type(ColliderType) on a nil value");
            }
        }

        LuaScriptMgr.Push(L, obj.type);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_offset(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf) o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name offset");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index offset on a nil value");
            }
        }

        LuaScriptMgr.Push(L, obj.offset);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int get_size(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf)o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name size");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index size on a nil value");
            }
        }

        LuaScriptMgr.Push(L, obj.size);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_radius(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf) o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name radius");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index radius on a nil value");
            }
        }

        LuaScriptMgr.Push(L, obj.radius);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int get_pathCount(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf)o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name pathCount");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index pathCount on a nil value");
            }
        }

        LuaScriptMgr.Push(L, obj.pathCount);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int set_pathCount(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf)o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name pathCount");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index pathCount on a nil value");
            }
        }

        obj.pathCount = (int)LuaScriptMgr.GetNumber(L, 3);
        return 0;
    }


    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int set_radius(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf)o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name radius");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index radius on a nil value");
            }
        }

        obj.radius = (float) LuaScriptMgr.GetNumber(L, 3);
        return 0;
    }


    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int set_size(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf)o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name size");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index size on a nil value");
            }
        }

        obj.size = LuaScriptMgr.GetVector2(L, 3);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int set_offset(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf) o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name offset");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index offset on a nil value");
            }
        }

        obj.offset = LuaScriptMgr.GetVector2(L, 3);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int set_type(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        ColliderConf obj = (ColliderConf) o;

        if (obj == null)
        {
            LuaTypes types = LuaDLL.lua_type(L, 1);
            if (types == LuaTypes.LUA_TTABLE)
            {
                LuaDLL.luaL_error(L, "unknown member name type(ColliderType)");
            }
            else
            {
                LuaDLL.luaL_error(L, "attempt to index type(ColliderType) on a nil value");
            }
        }

        obj.type = (ColliderType) LuaScriptMgr.GetVarObject(L, 3);
        return 0;
    }
}