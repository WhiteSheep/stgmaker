﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.Collision;

public class SEFColliderTypeWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] enums = new LuaMethod[]
        {
            new LuaMethod("Circle", GetCircle),
            new LuaMethod("Box", GetBox),
            new LuaMethod("Polygon", GetPolygon),
            new LuaMethod("Edge", GetEdge),
            new LuaMethod("IntToEnum", IntToEnum), 
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFColliderType", typeof(ColliderType), enums);
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetCircle(IntPtr L)
    {
        LuaScriptMgr.Push(L, ColliderType.Circle);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetBox(IntPtr L)
    {
        LuaScriptMgr.Push(L, ColliderType.Box);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetPolygon(IntPtr L)
    {
        LuaScriptMgr.Push(L, ColliderType.Polygon);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetEdge(IntPtr L)
    {
        LuaScriptMgr.Push(L, ColliderType.Edge);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int IntToEnum(IntPtr L)
    {
        int arg0 = (int) LuaDLL.lua_tonumber(L, 1);
        ColliderType o = (ColliderType) arg0;
        LuaScriptMgr.Push(L, o);
        return 1;

    }



}