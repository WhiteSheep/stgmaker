﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using LuaInterface;
using UnityEngine;
using EventType = StgMaker.Core.EventType;

public class SEFEventTypeWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] enums = new LuaMethod[]
        {
            new LuaMethod("NoEvent", GetNoEvent),
            new LuaMethod("Period", GetPeriod),
            new LuaMethod("OnFrameArrive", GetOnFrameArrive),
            new LuaMethod("EveryFrame", GetEveryFrame),
            new LuaMethod("SpriteInitialization", GetSpriteInitialization),
            new LuaMethod("OnKeyDown", GetOnKeyDown),
            new LuaMethod("OnKeyPressed", GetOnKeyPressed), 
            new LuaMethod("OnSpriteUnderAttack", GetOnSpriteUnderAttack), 
            new LuaMethod("OnSpriteHpChanged", GetOnSpriteHpChanged),
            new LuaMethod("OnSpriteDestroying", GetOnSpriteDestroying),
            new LuaMethod("OnSpriteDestroyed", GetOnSpriteDestroyed), 
            new LuaMethod("OnSpriteLifeTimeElapse", GetOnSpriteLifeTimeElapse), 
            new LuaMethod("IntToEnum", IntToEnum), 
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFEventType", typeof (StgMaker.Core.EventType), enums);
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetNoEvent(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventNoEvent);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetPeriod(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventPeriod);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetOnFrameArrive(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventOnFrameArrive);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetEveryFrame(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventEveryFrame);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetSpriteInitialization(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventSpriteInitialization);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetOnKeyDown(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventOnKeyDown);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetOnKeyPressed(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventOnKeyPressed);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetOnSpriteUnderAttack(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventOnSpriteUnderAttack);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetOnSpriteHpChanged(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventOnSpriteHpChanged);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetOnSpriteDestroying(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventOnSpriteDestroying);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetOnSpriteDestroyed(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventOnSpriteDestroyed);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int GetOnSpriteLifeTimeElapse(IntPtr L)
    {
        LuaScriptMgr.Push(L, EventType.EventOnSpriteLifeTimeElapse);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    private static int IntToEnum(IntPtr L)
    {
        int arg0 = (int) LuaDLL.lua_tonumber(L, 1);
        EventType o = (EventType) arg0;
        LuaScriptMgr.Push(L, o);
        return 1;
    }
}