﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.InputModule;

class SEFInputManagerWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] regs = new LuaMethod[]
        {
            new LuaMethod("GetButtonDown", GetButtonDown),
            new LuaMethod("GetButtonPressed", GetButton), 
        };

        LuaField[] fields = new LuaField[]
        {
            
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFInputManager", typeof(InputManager), regs, fields, typeof(object));
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetButtonDown(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 1);
        string btnName = LuaScriptMgr.GetLuaString(L, 1);

        bool ifBtnDown = InputManager.GetButtonDown(btnName);
        LuaScriptMgr.Push(L, ifBtnDown);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetButton(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 1);
        string btnName = LuaScriptMgr.GetLuaString(L, 1);

        LuaScriptMgr.Push(L, InputManager.GetButton(btnName));
        return 1;
    }

}