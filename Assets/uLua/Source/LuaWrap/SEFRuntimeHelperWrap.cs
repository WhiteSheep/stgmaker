﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.Core;

class SEFRuntimeHelperWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] regs = new LuaMethod[]
        {
            new LuaMethod("GetSpriteObjectByID", GetSpriteObjectByID), 
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFRuntimeHelper", regs);
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetSpriteObjectByID(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 1);

        int spriteId = Convert.ToInt32(LuaScriptMgr.GetNumber(L, 1));

        System.Object obj = Locator.GetSpriteMgr().GetSpriteByUID(spriteId);
        LuaScriptMgr.PushObject(L, obj);

        return 1;
    }
}