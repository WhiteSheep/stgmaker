﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.Core;

public class SEFSpriteObjectCampTypeWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] enums = new LuaMethod[]
        {
            new LuaMethod("Opponent",GetOpponent),
            new LuaMethod("Confederate", GetConfederate), 
            new LuaMethod("Neutral",GetNeutral), 
            new LuaMethod("IntToEnum", IntToEnum), 
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFSpriteObjectCampType", typeof(SpriteObjectCampType), enums);
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetOpponent(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectCampType.Opponent);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetConfederate(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectCampType.Confederate);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetNeutral(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectCampType.Neutral);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int IntToEnum(IntPtr L)
    {
        int o = (int) LuaDLL.lua_tonumber(L, 1);
        SpriteObjectCampType obj = (SpriteObjectCampType) o;
        LuaScriptMgr.Push(L, obj);
        return 1;
    }

    
}