﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.Core;
using UnityEngine;

public class SEFSpriteObjectTypeWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] enums = new LuaMethod[]
        {
             new LuaMethod("EnemySprite", GetEnemySprite),
             new LuaMethod("EnemyBullet", GetEnemyBullet),
             new LuaMethod("PlayerSprite", GetPlayerSprite),
             new LuaMethod("PlayerBullet", GetPlayerBullet),
             new LuaMethod("SpriteUnit", GetSpriteUnit),
             new LuaMethod("SpriteEffect", GetSpriteEffect), 
             new LuaMethod("Unknown", GetUnknown),
             new LuaMethod("IntToEnum", IntToEnum), 
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFSpriteObjectType", typeof(SpriteObjectType), enums);
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetEnemySprite(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectType.EnemySprite);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetEnemyBullet(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectType.EnemyBullet);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetPlayerSprite(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectType.PlayerSprite);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetPlayerBullet(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectType.PlayerBullet);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int GetUnknown(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectType.Unknown);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetSpriteUnit(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectType.SpriteUnit);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int GetSpriteEffect(IntPtr L)
    {
        LuaScriptMgr.Push(L, SpriteObjectType.SpriteEffect);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int IntToEnum(IntPtr L)
    {
        int arg0 = (int) LuaDLL.lua_tonumber(L, 1);
        SpriteObjectType o = (SpriteObjectType) arg0;
        LuaScriptMgr.Push(L, o);
        return 1;
    }


}