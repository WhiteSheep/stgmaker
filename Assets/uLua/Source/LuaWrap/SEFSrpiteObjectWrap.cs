﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using StgMaker.Core;
using UnityEngine;

internal class SEFSrpiteObjectWrap
{
    public static void Register(IntPtr L)
    {
        LuaMethod[] regs = new LuaMethod[]
        {
            new LuaMethod("New", New), 
            new LuaMethod("SetLocalRotation", SetLocalRotation),
            new LuaMethod("SetLocalScale", SetLocalScale),
            new LuaMethod("Rotate", Rotate),
        };

        LuaField[] fields = new LuaField[]
        {
            new LuaField("id", get_id, null),
            new LuaField("type", get_type, set_type),
            new LuaField("camp", get_camp, set_camp), 
            new LuaField("hp", get_hp, set_hp),
            new LuaField("atk", get_atk, set_atk),
            new LuaField("position", get_position, set_position), 
            new LuaField("maxLifeTime", get_maxLifeTime, set_maxLifeTime),
        };

        LuaScriptMgr.RegisterLib(L, "System.SEFSpriteObject", typeof (SpriteObject), regs, fields, typeof (object));
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int New(IntPtr L)
    {
        int count = LuaDLL.lua_gettop(L);

        if (count == 0)
        {
            SpriteObject obj = new SpriteObject();
            LuaScriptMgr.PushObject(L, obj);

            return 1;
        }

        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int SetLocalRotation(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 2);

        SpriteObject obj = (SpriteObject) LuaScriptMgr.GetVarObject(L, 1);
        Vector3 rotVec3 = LuaScriptMgr.GetVector3(L, 2);

        LuaScriptMgr.PushObject(L, obj.SetLocalRotation(rotVec3)); 
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int SetLocalScale(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 2);

        SpriteObject obj = (SpriteObject) LuaScriptMgr.GetVarObject(L, 1);
        Vector3 localScaleVec3 = LuaScriptMgr.GetVector3(L, 2);

        LuaScriptMgr.PushObject(L, obj.SetLocalScale(localScaleVec3));
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int Rotate(IntPtr L)
    {
        LuaScriptMgr.CheckArgsCount(L, 3);

        SpriteObject obj = (SpriteObject) LuaScriptMgr.GetVarObject(L, 1);
        Vector3 rotAxisVec3 = LuaScriptMgr.GetVector3(L, 2);
        float angle = (float) LuaScriptMgr.GetNumber(L, 3);

        LuaScriptMgr.PushObject(L, obj.Rotate(rotAxisVec3, angle));
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_type(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject)o;
        LuaWrapHelper.CheckLuaFieldValid(L, 1, "type(SpriteObjectType)");
        LuaScriptMgr.Push(L, obj.type);

        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int set_type(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject)o;
        LuaWrapHelper.CheckLuaFieldValid(L, 1, "type(SpriteObjectType)");
        obj.type = (SpriteObjectType)LuaScriptMgr.GetVarObject(L, 3);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int get_id(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject)o;

        LuaWrapHelper.CheckLuaFieldValid(L, 1, "id(int)");
        LuaScriptMgr.Push(L, obj.uid);
        return 1;
    }


    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_hp(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;

        LuaScriptMgr.Push(L, obj.hp);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int set_hp(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;

        obj.hp = Convert.ToInt32(LuaScriptMgr.GetNumber(L, 3));
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_position(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;
        LuaWrapHelper.CheckLuaFieldValid(L, 1, "position(vector2)");
        
        LuaScriptMgr.Push(L, obj.pos);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int set_position(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;
        LuaWrapHelper.CheckLuaFieldValid(L, 1, "position(vector2)");

        obj.pos = LuaScriptMgr.GetVector2(L, 3);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_atk(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;
        LuaWrapHelper.CheckLuaFieldValid(L, 1, "atk(int)");
        LuaScriptMgr.Push(L, obj.atk);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int set_atk(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;
        LuaWrapHelper.CheckLuaFieldValid(L, 1, "atk(int)");

        obj.atk = (int) LuaScriptMgr.GetNumber(L, 3);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_maxLifeTime(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;

        LuaWrapHelper.CheckLuaFieldValid(L, 1, "maxLifeTime(int)");
        LuaScriptMgr.Push(L, obj.maxLifeTime);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int set_maxLifeTime(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject)o;

        LuaWrapHelper.CheckLuaFieldValid(L, 1, "maxLifeTime(int)");
        obj.maxLifeTime = (int) LuaScriptMgr.GetNumber(L, 3);
        return 0;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int get_camp(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;

        LuaWrapHelper.CheckLuaFieldValid(L, 1, "camp(SpriteObjectCampType)");
        LuaScriptMgr.Push(L, obj.camp);
        return 1;
    }

    [MonoPInvokeCallbackAttribute(typeof (LuaCSFunction))]
    static int set_camp(IntPtr L)
    {
        object o = LuaScriptMgr.GetVarObject(L, 1);
        SpriteObject obj = (SpriteObject) o;

        LuaWrapHelper.CheckLuaFieldValid(L, 1, "camp(SpriteObjectCampType)");
        obj.camp = (SpriteObjectCampType) LuaScriptMgr.GetVarObject(L, 3);

        return 0;
    }

    
}