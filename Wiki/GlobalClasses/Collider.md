# Collider
> Class in StgMaker.Collision

## Description（描述）
- - -
Collider（碰撞体）用于描述精灵对象在游戏中的碰撞体积

	注意：
	1. 在本系统中1 = 100像素，因此 0.01 = 1像素
	2. 一个碰撞体类只能包含一种类型的碰撞体，由type字段决定。如果想让一个精灵对象拥有多个不同
		类型碰撞体，需要在脚本中创建多个碰撞体类，然后都绑定在精灵对象上
## Variables（变量）
- - -
> | 变量名         | 类型 | 权限 | 描述           |
> |:--------------|:----|:--|:-------------|
> | type            | ColliderType | 读写 | 碰撞体的类型 |
> | offset            | Vector2 | 读写 | 碰撞体相对于精灵对象锚点的偏移 |
> | size           | Vector2 | 读写 | 矩形碰撞体的大小 |
> | radius   | float | 读写 | 圆形碰撞体的半径 |
> | pathCount      | int | 读写 | 路径数，用于多边形和折线碰撞体|

## Constructors（构造方法）
- - -

### Collision.New
```lua
Collision New()
```

#### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | null | null|

## Public Functions（公共方法）
- - -

### Collider.AddPathPoints
```lua 
public void AddPathPoints(int index, Vector2 ... points) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | index            | 多边形碰撞体index，从0开始 |
> | points | 不定参数，多边形碰撞体的顶点信息 |
>
> #### Description（描述）
> 向碰撞体类中添加多边形碰撞体，首先要确定该碰撞体的编号（因为一个碰撞体类中可以存放多个多边形碰撞体），然后将这个多边形的所有顶点输入，从而完成碰撞体的生成
> 
```lua
--	该样例代码中创建了个三角形碰撞体
local collider = Collider.New()
collider.type = ColliderType.Polygon
collider.pathCount = 1
collider:AddPathPoints(1, Vector2.New(-1, 1), Vector2.New(1, 1), Vector2.New(0, -1))
```

### SpriteObject.AddEdgePoints
```lua 
public void AddEdgePoints(Vector2 ... points) 
```

> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | points            | 不定参数，折线的点信息 |
>
> #### Description（描述）
> 向碰撞体中添加折线碰撞体信息，输入参数是折线的弯折部分点
> 
```lua
-- 该样例代码中创建了个“V字型”折线碰撞体
local collider = Collider.New()
collider.type = ColliderType.Edge
collider:AddEdgePoints(1, Vector2.New(-1, 1), Vector2.New(0, -1), Vector2.New(1, 1))
```


