# InputManager
> Class in StgMaker.InputModule

## Description（描述）
- - -
InputManager（输入管理器），用于获取系统当前的用户输入信息

## Variables（变量）
- - -
> 无

## Constructors（构造方法）
- - -
> 无

## Public Functions（公共方法）
- - -
> 无

## Static Functions （静态方法）
- - -
### InputManager.GetButtonDown
```lua 
public bool GetButtonDown (BuildInVirtualButton btn) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | btn            | 内建虚拟按键 |
>
> #### Description（描述）
> 当虚拟按键被按下时，该方法返回true（该方法仅在按键被按下的那一帧返回true，如果是捕捉按住后的信息请使用GetButtonPressed方法）
> 
```lua
if Input.GetButtonDown(BuildInVirtualButton.Left) then
	print("VirtualButton.Left is press down")
end
```

### InputManager.GetButtonPressed
```lua 
public bool GetButtonPressed(BuildInVirtualButton btn) 
```

> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | btn            | 内建虚拟按键 |
>
> #### Description（描述）
> 当虚拟按键被持续按住时，该方法返回true
> 
```lua
if Input.GetButtonPressed(BuildInVirtualButton.Left) then
	print("VirtualButton.Left is press down")
end
```


