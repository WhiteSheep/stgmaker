# LuaExp
> Class in StgMaker.Core

## Description（描述）
- - -
LuaExp向CL层提供了维持系统运转的重要接口

	注意：在一般情况下，开发者没有必要直接使用LuaExp下的接口，这些信息会被CL层包装后进一步的提供给开发者使用
	（如果一定要使用，请确保你知道这个接口在做什么）


## Variables（变量）
- - -
> 无

## Constructors（构造方法）
- - -
> 无

## Public Functions（公共方法）
- - -
> 无

## Static Functions （静态方法）
- - -
### LuaExp.ShowLog
```lua 
public void ShowLog (string log) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | log            | 日志信息 |
>
> #### Description（描述）
> 打印Log信息
> 
```lua
LuaExp.ShowLog("This is only a test info~")
end
```

### LuaExp.CreateUID
```lua 
public int CreateUID() 
```

> #### Parameters（参数）
	无
>
> #### Description（描述）
> 调用该方法生成全局唯一的ID
> 
```lua
local uniqueId = LuaExp.CreateUID()
```

### LuaExp.RegistTrigger 
```lua 
public void RegistTrigger(LuaTable triggerTab) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | triggerTab           | trigger的LuaTable信息 |
>
> #### Description（描述）
> 向引擎系统中注册Trigger
> 
```lua
local trigger = Trigger.Create()
--	xxx code 完成trigger构建

LuaExp.RegistTrigger(trigger)
```

### LuaExp.RegistSpriteObjectWrapper
```lua 
public void RegistSpriteObjectWrapper (LuaTable wrapperTab) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | wrapperTab            | 精灵对象包裹LuaTable |
>
> #### Description（描述）
> 将一个SpriteObjectWrapper类注册到系统中，并让系统解析这个Wrapper添加到游戏中
> 
```lua
local wrapper = SpriteObjectWrapper.New()
-- 省略初始化Wrapper代码
LuaExp.RegistSpriteObjectWrapper(wrapper)
```

### LuaExp.DoFile
```lua
public object[] DoFile (string filePath) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | filePath            | 待运行的Lua文件地址 |
>
> #### Description（描述）
> 该方法用于运行一个Lua文件，并且将该文件的运行结果返回，返回类型是object数组（下标从0开始）
> 常用于运行关卡文件以外的其他Lua文件
```lua
local retValue = LuaExp.DoFile("DemoStage\\Sprite_01.lua")
```

### LuaExp.LoadTexture2D
```lua 
public void LoadTexture2D (string tex2DFilemame, string id) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | tex2DFilemame    | Texture2D贴图文件名 |
>
> #### Description（描述）
> 从文件中读取一个材质信息载入到系统中，之后在任何地方调用该素材均使用id作为索引
	
	注意： 读取素材不需要写具体的文件路径，通过配置系统运行环境的相关参数后，该方法可以自动寻找匹配的文件信息
```lua
CL.LoadTexture2D("player", "pl00.png")
CL.LoadTexture2D("enemy", "enemy.png")
CL.LoadTexture2D("explode", "boom3_0.png")
```

### LuaExp.RegistSystemEnv
```lua 
public void RegistSystemEnv (ENVItem envItem, string value) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | envItem       | 需要注册的环境字段 |
> | value         | 注册值        |
>
> #### Description（描述）
> 注册系统环境信息，可以注册的有效环境字段请参考ENVItem的相关信息
> 
```lua
LuaExp.RegistSystemEnv(Env.ProjectPath, "DemoStage/")
```

### LuaExp.GetFrameCount
```lua 
public int GetFrameCount () 
```
>
> #### Parameters（参数）
	无
>
> #### Description（描述）
> 获取当前系统运行的帧数信息
> 
```lua
print("当前系统已经运行", LuaExp.GetFrameCount(), "帧")
end
```

