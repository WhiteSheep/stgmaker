# SpriteObject
> Class in StgMaker.Core

## Description（描述）
- - -
SpriteObject（精灵对象）是游戏场景中所有单位的基础

## Variables（变量）
- - -
> | 变量名         | 类型 | 权限 | 描述           |
> |:--------------|:----|:--|:-------------|
> | id            | int | 只读 |ID信息，用于区分精灵的唯一标识 |
> | hp            | int | 读写 |精灵的生命值 |
> | atk           | int | 读写 |精灵的攻击力 |
> | maxLifeTime   | int | 读写 |精灵的最大存活时间，正整数有效，0或负数则视为无限 |
> | position      | Vector2 | 读写 |精灵当前的坐标位置 |
> | type          | SpriteObjectType | 读写 |精灵的类型 |

## Constructors（构造方法）
- - -

### SpriteObject.New
```lua
SpriteObject New()
```

#### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | null | null|

## Public Functions（公共方法）
- - -

### SpriteObject.SetLocalRotation
```lua 
public SpriteObject SetLocalRotation(Vector3 rotVec3) 
```
>
> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | rotVec3            | 旋转角度（弧度） |
>
> #### Description（描述）
> 设置一个精灵对象的旋转值（会影响到碰撞判定），并返回精灵对象本身
> 
```lua
local playerObj = SpriteObject.New() 
playerObj:SetLocalRotation(Vector3.New(0, 0, 1.414)) 
```

### SpriteObject.SetLocalScale
```lua 
public SpriteObject SetLocalScale(Vector3 scaVec3) 
```

> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | scaVec3            | 缩放大小 |
>
> #### Description（描述）
> 设置一个精灵对象的缩放大小（会影响到碰撞判定），并返回精灵对象本身
> 
```lua
local playerObj = SpriteObject.New()
playerObj:SetLocalScale(Vector3.New(2, 2, 1)) 
```

### SpriteObject.Rotate
```lua
public SpriteObject Rotate(Vector3 axisVec3, float angle)
```

> #### Parameters（参数）
> | 变量名         |  描述      |
> | :-------------|:-------------|
> | axisVec3      | 旋转轴 |
> | angle         | 旋转弧度数 |
>
> #### Description（描述）
> 沿着一个具体的轴将精灵对象旋转一个弧度（会影响到碰撞判定），并返回精灵对象本身


```lua
local playerObj = SpriteObject.New()
playerObj:Rotate(Vector3.Front, 0.6) 
```

