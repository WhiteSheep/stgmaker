# BuildInAnimationControllerType
> enumeration in StgMaker.Core

## Description（描述）
- - -
BuildInAnimationControllerType（内建动画控制器状态）包含了系统内建的所有动画控制器类型

## Variables（变量）
- - -
> | 变量名         | 描述           |
> |:--------------|:-------------|
> | SimpleSprite    | 一般精灵，包含Idle、ToLeft、Left、ToRight、Right五种状态，通常用于SpriteUnit |
> | SpriteEffect  | 精灵特效，只包含Effect状态，通常用于SpriteEffect |




