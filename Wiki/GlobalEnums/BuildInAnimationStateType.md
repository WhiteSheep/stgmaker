# BuildInAnimationStateType
> enumeration in StgMaker.Core

## Description（描述）
- - -
BuildInAnimationStateType（内建动画状态）提供了系统支持的所有内建动画状态机中所支持的状态类型，这些状态类型的应用请参考BuildInAnimationControlerType（内建动画控制器类型）中的相关描述

## Variables（变量）
- - -
> | 变量名         | 描述           |
> |:--------------|:-------------|
> | Idle    | 静止状态 |
> | ToLeft  | 精灵向左运动转换状态 |
> | Left    | 精灵持续向左运动状态 |
> | ToRight | 精灵持向右运动转换状态 |
> | Right   | 精灵持续向右运动状态 |
> | Effect  | 特效播放状态        |




