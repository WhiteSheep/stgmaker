# BuildInVirtualButton
> enumeration in StgMaker.InputModule

## Description（描述）
- - -
BuildInVirtualButton（内建虚拟按键）包含了系统内支持的所有虚拟按键类型

## Variables（变量）
- - -
> | 变量名         | 描述           |
> |:--------------|:-------------|
> | Up    | 上方向键 |
> | Down  | 下方向键 |
> | Left  | 左方向键 |
> | Right | 右方向键 |
> | Attack| 攻击/确定 |
> | Slow  | 减速 |
> | Bomb  | 炸弹/取消 |
> | Empty0 | 额外0 |
> | Empty1 | 额外1 |
> | Empty2 | 额外2 |
> | Empty3 | 额外3 |
> | Empty4 | 额外4 |
> | Empty5 | 额外5 |
> | Empty6 | 额外6 |
> | Empty7 | 额外7 |
> | Empty8 | 额外8 |
> | Empty9 | 额外9 |




