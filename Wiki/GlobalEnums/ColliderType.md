# ColliderType
> enumeration in StgMaker.Collision

## Description（描述）
- - -
ColliderType（碰撞体类型）包含了系统内支持的所有碰撞体类型

## Variables（变量）
- - -
> | 变量名         | 描述           |
> |:--------------|:-------------|
> | Circle    | 圆形碰撞体 |
> | Box  | 矩形碰撞体 |
> | Polygon  | 多边形碰撞体 |
> | Edge | 折线碰撞体 |





