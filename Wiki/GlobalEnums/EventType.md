# EventType
> enumeration in StgMaker.Core

## Description（描述）
- - -
EventType（事件类型）包含了系统内支持的所有事件类型

## Variables（变量）
- - -
> | 变量名         | 描述           |
> |:--------------|:-------------|
> | Period    | 周期性触发事件 |
> | OnFrameArrive  | 当前游戏关卡运行到某一帧触发 |
> | EveryFrame  | 每一帧都会触发事件 |
> | SpriteInitialization | 当精灵对象初始化时触发 |
> | OnKeyDown | 当某个按键被按下时触发 |
> | OnKeyPressed | 当某个按键被持续按住时触发  |
> | OnSpriteUnderAttack | 当精灵对象遭受攻击时触发（有可能不扣除生命值） |
> | OnSpriteHpChanged | 当精灵对象生命值发生改变时触发 |
> | OnSpriteDestroying | 当精灵对象即将被摧毁时触发 |
> | OnSpriteLifeTimeElapse | 当精灵对象的生存一定帧数时触发 |

## Enum Details（枚举信息细节）
- - -

### Period

> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | periodFrame| int | 两次事件触发的间隔时间 |

### OnFrameArrive
> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | arriveFrame | int | 当一个关卡运行到该帧时触发事件 |

### EveryFrame
> 无参数

### SpriteInitialization
> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | spriteId | int | 监测精灵对象的id |

### OnKeyDown

> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | keycode | KeyCode | 当该按键被按下时会触发 |

### OnKeyPressed

> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | keycode | KeyCode | 当该按键被持续按住时会触发 |

### OnSpriteUnderAttack
> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | spriteId | int | 监测精灵对象的id |

### OnSpriteHpChanged
> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | spriteId | int | 监测精灵对象的id |

### OnSpriteDestroying
> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | spriteId | int | 监测精灵对象的id |

### OnSpriteLifeTimeElapse
> | 参数名 | 参数类型 | 参数含义                  |
> |:------|:--------|:-------------------------|
> | spriteId | int | 监测精灵对象的id |
> | elapsedFrame| int | 被监测精灵对象存活超过该时间后会触发 |

