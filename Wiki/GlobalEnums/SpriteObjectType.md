# SpriteObjectType
> enumeration in StgMaker.Core

## Description（描述）
- - -
SpriteObjectType（精灵对象类型）用于描述精灵对象在具体应用时的类型

## Variables（变量）
- - -
> | 变量名         | 描述           |
> |:--------------|:-------------|
> | SpriteUnit    | 精灵单位，常用于游戏关卡中的单位（自机、敌机、子弹等）|
> | SpriteEffect  | 精灵特效，常用语游戏关卡中的2D帧动画特效 |


