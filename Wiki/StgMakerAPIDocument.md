
# Welcome to the StgMaker Scripting Reference!
---
	
	该部分文档包含了StgMaker所提供的所有脚本API的细节。为了能够更方便的理解这些信息，你需要掌握StgMaker中的基础概念和一些常用的使用方法后，再来阅读这些内容

	Scripting Reference的构成目前分为三个部分：
	1. Kernel Wrap Classes : 引擎直接提供的类
	2. Kernel Wrap Enums : 引擎直接提供的枚举
	3. Lua Classes : 采用Lua语言实现的类

	每个部分可以通过目录找到相关的信息，点击跳转到详细介绍页面。

	为了帮助开发者更好的理解程序，对于类的每一个成员方法、成员变量等内容的描述，手册都会附上样例代码助于理解

	最后，也是最关键的，在使用这些方法时,请引用StgMaker\\CLStdLib库，这个库是支撑所有Lua代码运转的核心库

> - - -
> [TOC]
> - - -

## Kernel Wrap Classes
### [SpriteObject](GlobalClasses/SpriteObject.md)
### [InputManager](GlobalClasses/InputManager.md)
### [LuaExp](GlobalClasses/LuaExp.md)
### [Collider](GlobalClasses/Collider.md)

## Kernel Wrap Enums
### [SpriteObjectType](GlobalEnums/SpriteObjectType.md)
### [BuildInAnimationControllerType](GlobalEnums/BuildInAnimationControllerType.md)
### [BuildInAnimationStateType](GlobalEnums/BuildInAnimationStateType.md)
### [BuildInVirtualButton](GlobalEnums/BuildInVirtualButton.md)
### [ColliderType](GlobalEnums/ColliderType.md)
### [EventType](GlobalEnums/EventType.md)

## Lua Classes